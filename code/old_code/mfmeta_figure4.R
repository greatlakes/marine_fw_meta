# Figure 4 Marine-Freshwater meta-analysis
#
# Objecitves:  
# 1. to identify shared nodes between marine and freshwater samples
# 2. to visualize how the number of shared nodes increase with number
# of sites sampled
#
# This analysis is based on MED nodes
#
# contrubutors:  S. Paver
# last updated:  18 April 2017, SFP
#
##########################################################################

# load necessary packages
library(dplyr)
library(phyloseq)
library(ggplot2)
library(stringr)
library(ape)
library(car)
library(vegan)
library(reshape)
library(gridExtra)
library(plyr)
library(scales)

# set the theme to classic
theme_set(theme_classic())

##Analysis based on MED nodes (first pass only focusing on bacteria)

#read in the tree file
treefile = "~/repos/marine_fw_meta/data/marine_fw4.fasttree"
read.tree(file=treefile) -> tree
phy_tree(tree) -> tree.phy

#read in MED table
MED_count_table = "~/repos/marine_fw_meta/data/MED.count_table"
read.table(MED_count_table, header=TRUE, row.names = 1) -> MED.otu
rownames(MED.otu) -> nodenames

otu_table(MED.otu, taxa_are_rows = TRUE) -> MED.phylo

#taxonomy files
node_tax_file = "~/repos/marine_fw_meta/data/MED_nodes.nr_v128.wang.taxonomy_mod.txt"
#note:  the original taxonomy file from mothur was modified in excel by moving taxonomy to column C,
#using a "|" delimiter to separate OTU names and size; adding row one headers:  "OTU", "size", "taxonomy"

import_mothur(mothur_constaxonomy_file = node_tax_file) -> tax.tab
rownames(tax.tab) -> oligonames
cbind(tax.tab, oligonames) -> tax.tab2
tax_table(tax.tab2) -> tax.phylo

##Parsing tip labels on tree for taxon and number
##Reformatting sample number to match abundance matrix
taxa<-c(NA)
for(i in 1:length(tree.phy$tip.label)){
  a<-strsplit(tree.phy$tip.label[i],split = '\\|')
  taxa[i]<-a[[1]][2]
  tree.phy$tip.label[i]<-paste0('X',a[[1]][1])
}

##Reformatting tax.phylo names
fillerfunction<-function(name){
  target_length<-10
  counter<-str_count(name)
  z<-target_length-(counter+1)
  os<-rep('0',z)
  os<-paste0(os,collapse='')
  result<-paste0('X',os,name)
  return(result)
}

rownames(tax.phylo)->tpnames
tpnewnames<-c(NA)
for(i in 1:length(tpnames)){
  fillerfunction(tpnames[i])->tpnewnames[i]
}
tpnewnames->rownames(tax.phylo)

#Load table with sample data and create phyloseq sample table
sample.data<-read.table('~/repos/marine_fw_meta/data/sample_data.txt', header=TRUE, stringsAsFactors = FALSE)
sample_data(sample.data)-> sample.table

#merge files together!
merge_phyloseq(tree.phy, MED.phylo, tax.phylo, sample.table) -> bac

## separate sharednodes from marine and freshwater only nodes
subset_samples(bac, habitat1 == "freshwater") -> freshsamples
subset_samples(bac, habitat1 == "marine") -> marinesamples
freshsamples = prune_taxa(taxa_sums(freshsamples)>0, freshsamples)
marinesamples = prune_taxa(taxa_sums(marinesamples)>0, marinesamples)
rownames(tax_table(marinesamples)) -> mtaxa #all marine taxa
rownames(tax_table(freshsamples)) -> ftaxa #all freshwater taxa
intersect(mtaxa, ftaxa) -> sharedtaxa

#### classify shared nodes as 'shared' in phyloseq object

data.frame(tax_table(bac)) -> tt
rownames(tt) -> oligonames2
cbind(tt,oligonames2) -> tt2
tt2$oligonames2 <- recode(tt2$oligonames2, "sharedtaxa='shared'")
tax_table(as.matrix(tt2)) -> tax_table(bac)

subset_taxa(bac, oligonames2 == "shared") -> sharednodes

tax_table(sharednodes) -> sharednodes.taxonomy
phy_tree(sharednodes)-> sharednodes.tree
write.tree(sharednodes.tree, file = "~/repos/marine_fw_meta/results/sharednodes.tree", append = FALSE,
           digits = 10, tree.names = FALSE)
write.table(sharednodes.taxonomy, file = "~/repos/marine_fw_meta/results/sharednodes.taxonomy")

subset_taxa(bac, oligonames2 != "shared") -> uniquenodes
subset_samples(uniquenodes, habitat1 == "freshwater") -> fresh_samples
subset_samples(uniquenodes, habitat1 == "marine") -> marine_samples
fresh_only = prune_taxa(taxa_sums(fresh_samples)>0, fresh_samples)
marine_only = prune_taxa(taxa_sums(marine_samples)>0, marine_samples)

rownames(tax_table(marine_only)) -> motaxa
rownames(tax_table(fresh_only)) -> fotaxa
intersect(motaxa, fotaxa) -> mistakes

################# Species accumulation curves ################################
# Calculate # of shared taxa
# Visualize distribution of shared OTUs
##############################################################################

freshwater_shared = subset_samples(sharednodes, habitat1 == "freshwater")

f_alpha = subset_taxa(freshwater_shared, Rank3 == "Alphaproteobacteria")
f_beta = subset_taxa(freshwater_shared, Rank3 == "Betaproteobacteria")
f_gamma = subset_taxa(freshwater_shared, Rank3 == "Gammaproteobacteria")
f_cyano = subset_taxa(freshwater_shared, Rank2 == "Cyanobacteria")
f_bac = subset_taxa(freshwater_shared, Rank2 == "Bacteroidetes")
f_actino = subset_taxa(freshwater_shared, Rank2 == "Actinobacteria")
f_delta = subset_taxa(freshwater_shared, Rank3 == "Deltaproteobacteria")

# Phyla with <5 shared taxa (not included in analysis)
# Acidobacteria #1
# Marinimicrobia_(SAR406_clade) #3
# Euryarchaeota #2
# Thaumarchaeota #3
# Firmicutes #3
# Planctomycetes #3
# Omnitrophica #1
# Verrucomicrobia #3

t(otu_table(f_alpha)) -> falpha
t(otu_table(f_beta)) -> fbeta
t(otu_table(f_gamma)) -> fgamma
t(otu_table(f_cyano)) -> fcyano
t(otu_table(f_bac)) -> fbac
t(otu_table(f_actino)) -> factino
t(otu_table(f_delta)) -> fdelta

specaccum(falpha, method = "random", permutations = 100) -> falphaspec
specaccum(fbeta, method = "random", permutations = 100) -> fbetaspec
specaccum(fgamma, method = "random", permutations = 100) -> fgammaspec
specaccum(fcyano, method = "random", permutations = 100) -> fcyanospec
specaccum(fbac, method = "random", permutations = 100) -> fbacspec
specaccum(factino, method = "random", permutations = 100) -> factinospec
specaccum(fdelta, method = "random", permutations = 100) -> fdeltaspec

## repeat for marine samples

marine_shared = subset_samples(sharednodes, habitat1 == "marine")
t(otu_table(marine_shared)) -> com2

specaccum(com2, method = "random", permutations = 100) -> test2
summary(test2)
plot(test2, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col="lightblue")

m_alpha = subset_taxa(marine_shared, Rank3 == "Alphaproteobacteria")
m_beta = subset_taxa(marine_shared, Rank3 == "Betaproteobacteria")
m_gamma = subset_taxa(marine_shared, Rank3 == "Gammaproteobacteria")
m_cyano = subset_taxa(marine_shared, Rank2 == "Cyanobacteria")
m_bac = subset_taxa(marine_shared, Rank2 == "Bacteroidetes")
m_actino = subset_taxa(marine_shared, Rank2 == "Actinobacteria")
m_delta = subset_taxa(marine_shared, Rank3 == "Deltaproteobacteria")

t(otu_table(m_alpha)) -> malpha
t(otu_table(m_beta)) -> mbeta
t(otu_table(m_gamma)) -> mgamma
t(otu_table(m_cyano)) -> mcyano
t(otu_table(m_bac)) -> mbac
t(otu_table(m_actino)) -> mactino
t(otu_table(m_delta)) -> mdelta

specaccum(malpha, method = "random", permutations = 100) -> malphaspec
specaccum(mbeta, method = "random", permutations = 100) -> mbetaspec
specaccum(mgamma, method = "random", permutations = 100) -> mgammaspec
specaccum(mcyano, method = "random", permutations = 100) -> mcyanospec
specaccum(mbac, method = "random", permutations = 100) -> mbacspec
specaccum(mactino, method = "random", permutations = 100) -> mactinospec
specaccum(mdelta, method = "random", permutations = 100) -> mdeltaspec

########### function for making colors transparent #######################
t_col <- function(color, percent = 50, name = NULL) {
  rgb.val <- col2rgb(color)
  t.col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
               alpha = (100-percent)*255/100,
               names = name)
  invisible(t.col)
}
t_col("red") -> tred
t_col("lightblue") -> tblue
t_col("purple") -> tpurple
t_col("green") -> tgreen
t_col("orange") -> torange
t_col("cyan") -> tcyan
t_col("yellow") -> tyellow

### plot freshwater and marine shared node species accumulation curves in the same figure
par(mfrow=c(1,2))

plot(falphaspec, ci.type="poly", col="red", lwd=2, ci.lty=0, ci.col=tred, 
     ylim=c(1,75), xlim=c(1,50), xlab="# of freshwater sites", ylab="number of shared MED nodes")
plot(fbetaspec, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col=tblue, add = TRUE)
plot(fgammaspec, ci.type="poly", col="purple", lwd=2, ci.lty=0, ci.col=tpurple, add = TRUE)
plot(fcyanospec, ci.type="poly", col="darkgreen", lwd=2, ci.lty=0, ci.col=tgreen, add = TRUE)
plot(fbacspec, ci.type="poly", col="orange", lwd=2, ci.lty=0, ci.col=torange, add = TRUE)
plot(factinospec, ci.type="poly", col="cyan", lwd=2, ci.lty=0, ci.col=tcyan, add = TRUE)
plot(fdeltaspec, ci.type="poly", col="gold", lwd=2, ci.lty=0, ci.col=tyellow, add = TRUE)

plot(malphaspec, ci.type="poly", col="red", lwd=2, ci.lty=0, ci.col=tred, 
     ylim=c(1,75), xlim=c(1,50), xlab="# of marine sites", ylab="number of shared MED nodes")#,
     #yaxt='n', ann=FALSE) # suppresses yaxis labels
plot(mbetaspec, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col=tblue, add = TRUE)
plot(mgammaspec, ci.type="poly", col="purple", lwd=2, ci.lty=0, ci.col=tpurple, add = TRUE)
plot(mcyanospec, ci.type="poly", col="darkgreen", lwd=2, ci.lty=0, ci.col=tgreen, add = TRUE)
plot(mbacspec, ci.type="poly", col="orange", lwd=2, ci.lty=0, ci.col=torange, add = TRUE)
plot(mactinospec, ci.type="poly", col="cyan", lwd=2, ci.lty=0, ci.col=tcyan, add = TRUE)
plot(mdeltaspec, ci.type="poly", col="gold", lwd=2, ci.lty=0, ci.col=tyellow, add = TRUE)

#### create species accumulation curves for all nodes, not just shared nodes
t(otu_table(freshsamples)) -> fs
specaccum(fs, method = "random", permutations = 100) -> test
summary(test)
plot(test, ci.type="poly", col="darkgreen", lwd=2, ci.lty=0, ci.col="lightgreen")

ft_alpha = subset_taxa(freshsamples, Rank3 == "Alphaproteobacteria")
ft_beta = subset_taxa(freshsamples, Rank3 == "Betaproteobacteria")
ft_gamma = subset_taxa(freshsamples, Rank3 == "Gammaproteobacteria")
ft_cyano = subset_taxa(freshsamples, Rank2 == "Cyanobacteria")
ft_bac = subset_taxa(freshsamples, Rank2 == "Bacteroidetes")
ft_actino = subset_taxa(freshsamples, Rank2 == "Actinobacteria")
ft_delta = subset_taxa(freshsamples, Rank3 == "Deltaproteobacteria")

# Acidobacteria #1
# Marinimicrobia_(SAR406_clade) #3
# Euryarchaeota #2
# Thaumarchaeota #3
# Firmicutes #3
# Planctomycetes #3
# Omnitrophica #1
# Verrucomicrobia #3

t(otu_table(ft_alpha)) -> ftalpha
t(otu_table(ft_beta)) -> ftbeta
t(otu_table(ft_gamma)) -> ftgamma
t(otu_table(ft_cyano)) -> ftcyano
t(otu_table(ft_bac)) -> ftbac
t(otu_table(ft_actino)) -> ftactino
t(otu_table(ft_delta)) -> ftdelta


specaccum(ftalpha, method = "random", permutations = 100) -> ftalphaspec
specaccum(ftbeta, method = "random", permutations = 100) -> ftbetaspec
specaccum(ftgamma, method = "random", permutations = 100) -> ftgammaspec
specaccum(ftcyano, method = "random", permutations = 100) -> ftcyanospec
specaccum(ftbac, method = "random", permutations = 100) -> ftbacspec
specaccum(ftactino, method = "random", permutations = 100) -> ftactinospec
specaccum(ftdelta, method = "random", permutations = 100) -> ftdeltaspec


t_col <- function(color, percent = 50, name = NULL) {
  rgb.val <- col2rgb(color)
  t.col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
               alpha = (100-percent)*255/100,
               names = name)
  invisible(t.col)
}
t_col("red") -> tred
t_col("lightblue") -> tblue
t_col("purple") -> tpurple
t_col("green") -> tgreen
t_col("orange") -> torange
t_col("cyan") -> tcyan

plot(ftbacspec, ci.type="poly", col="orange", lwd=2, ci.lty=0, ci.col=torange, 
     ylim=c(1,1200), xlim=c(1,50), xlab="# of freshwater sites", ylab="number of shared MED nodes")
plot(ftbetaspec, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col=tblue, add = TRUE)
plot(ftgammaspec, ci.type="poly", col="purple", lwd=2, ci.lty=0, ci.col=tpurple, add = TRUE)
plot(ftcyanospec, ci.type="poly", col="darkgreen", lwd=2, ci.lty=0, ci.col=tgreen, add = TRUE)
plot(ftalphaspec, ci.type="poly", col="red", lwd=2, ci.lty=0, ci.col=tred, add = TRUE)
plot(ftactinospec, ci.type="poly", col="cyan", lwd=2, ci.lty=0, ci.col=tcyan, add = TRUE)
plot(ftdeltaspec, ci.type="poly", col="gold", lwd=2, ci.lty=0, ci.col=tyellow, add = TRUE)

t(otu_table(marinesamples)) -> ms
specaccum(ms, method = "random", permutations = 100) -> test2
summary(test2)
plot(test2, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col="lightblue")

mt_alpha = subset_taxa(marinesamples, Rank3 == "Alphaproteobacteria")
mt_beta = subset_taxa(marinesamples, Rank3 == "Betaproteobacteria")
mt_gamma = subset_taxa(marinesamples, Rank3 == "Gammaproteobacteria")
mt_cyano = subset_taxa(marinesamples, Rank2 == "Cyanobacteria")
mt_bac = subset_taxa(marinesamples, Rank2 == "Bacteroidetes")
mt_actino = subset_taxa(marinesamples, Rank2 == "Actinobacteria")
mt_delta = subset_taxa(marinesamples, Rank3 == "Deltaproteobacteria")

t(otu_table(mt_alpha)) -> mtalpha
t(otu_table(mt_beta)) -> mtbeta
t(otu_table(mt_gamma)) -> mtgamma
t(otu_table(mt_cyano)) -> mtcyano
t(otu_table(mt_bac)) -> mtbac
t(otu_table(mt_actino)) -> mtactino
t(otu_table(mt_delta)) -> mtdelta


specaccum(mtalpha, method = "random", permutations = 100) -> mtalphaspec
specaccum(mtbeta, method = "random", permutations = 100) -> mtbetaspec
specaccum(mtgamma, method = "random", permutations = 100) -> mtgammaspec
specaccum(mtcyano, method = "random", permutations = 100) -> mtcyanospec
specaccum(mtbac, method = "random", permutations = 100) -> mtbacspec
specaccum(mtactino, method = "random", permutations = 100) -> mtactinospec
specaccum(mtdelta, method = "random", permutations = 100) -> mtdeltaspec

plot(mtalphaspec, ci.type="poly", col="red", lwd=2, ci.lty=0, ci.col=tred, 
     ylim=c(1,1200), xlim=c(1,50), xlab="# of marine sites", ylab="number of shared MED nodes")
plot(mtbetaspec, ci.type="poly", col="blue", lwd=2, ci.lty=0, ci.col=tblue, add = TRUE)
plot(mtgammaspec, ci.type="poly", col="purple", lwd=2, ci.lty=0, ci.col=tpurple, add = TRUE)
plot(mtcyanospec, ci.type="poly", col="darkgreen", lwd=2, ci.lty=0, ci.col=tgreen, add = TRUE)
plot(mtbacspec, ci.type="poly", col="orange", lwd=2, ci.lty=0, ci.col=torange, add = TRUE)
plot(mtactinospec, ci.type="poly", col="cyan", lwd=2, ci.lty=0, ci.col=tcyan, add = TRUE)
plot(mtdeltaspec, ci.type="poly", col="gold", lwd=2, ci.lty=0, ci.col=tyellow, add = TRUE)

######### create table with # shared nodes, # total nodes, %

perspec <- function(sh, totes){
  sh$richness/totes$richness
}
Alphaproteobacteria <- perspec(falphaspec, ftalphaspec)
Betaproteobacteria <- perspec(fbetaspec, ftbetaspec)
Bacteroidetes <- perspec(fbacspec, ftbacspec)
Cyanobacteria <- perspec(fcyanospec, ftcyanospec)
Gammaproteobacteria <- perspec(fgammaspec, ftgammaspec)
Actinobacteria <- perspec(factinospec, ftactinospec)
Deltaproteobacteria <- perspec(fdeltaspec, ftdeltaspec)

cbind(falphaspec$richness, fbetaspec$richness, fbacspec$richness, fcyanospec$richness, fgammaspec$richness, factinospec$richness, fdeltaspec$richness) -> fw_shared
cbind(ftalphaspec$richness, ftbetaspec$richness, ftbacspec$richness, ftcyanospec$richness, ftgammaspec$richness, ftactinospec$richness, ftdeltaspec$richness) -> fw_total
cbind(Alphaproteobacteria,Betaproteobacteria, Bacteroidetes,Cyanobacteria, Gammaproteobacteria, Actinobacteria, Deltaproteobacteria) -> fw_quest

mAlphaproteobacteria <- perspec(malphaspec, mtalphaspec)
mBetaproteobacteria <- perspec(mbetaspec, mtbetaspec)
mBacteroidetes <- perspec(mbacspec, mtbacspec)
mCyanobacteria <- perspec(mcyanospec, mtcyanospec)
mGammaproteobacteria <- perspec(mgammaspec, mtgammaspec)
mActinobacteria <- perspec(mactinospec, mtactinospec)
mDeltaproteobacteria <- perspec(mdeltaspec, mtdeltaspec)

cbind(mAlphaproteobacteria,mBetaproteobacteria, mBacteroidetes,mCyanobacteria, mGammaproteobacteria, mActinobacteria, mDeltaproteobacteria) -> m_quest
cbind(malphaspec$richness, mbetaspec$richness, mbacspec$richness, mcyanospec$richness, mgammaspec$richness, mactinospec$richness, mdeltaspec$richness) -> m_shared
cbind(mtalphaspec$richness, mtbetaspec$richness, mtbacspec$richness, mtcyanospec$richness, mtgammaspec$richness, mtactinospec$richness, mtdeltaspec$richness) -> m_total

melt(fw_quest) -> mfquest
melt(fw_shared) -> mfshared
melt(fw_total) -> mftotal

melt(m_quest) -> mmquest
melt(m_shared) -> mmshared
melt(m_total) -> mmtotal

colnames(mfquest) <- c("X1", "Phylum", "value")
colnames(mfshared) <- c("X2", "P1", "shared")
colnames(mftotal) <- c("X3", "P2", "total")
cbind(mfquest, mfshared, mftotal) -> fresh_stats
cbind(filter(fresh_stats, X1 == "44"), "freshwater") -> fstats

colnames(mmquest) <- c("X1", "Phylum", "value")
colnames(mmshared) <- c("X2", "P1", "shared")
colnames(mmtotal) <- c("X3", "P2", "total")
cbind(mmquest, mmshared, mmtotal) -> m_stats
cbind(filter(m_stats, X1 == "32"), "marine") -> mstats

cbind(fstats, mstats)[,c(2,3,6,9,10, 13,16,19)] -> stats_table


p1 <- ggplot(mfquest, aes(x=X1, y=value)) + geom_point(data=mfquest, aes(col=Phylum)) + ylim(0,0.5) +
  ylab("% of freshwater nodes shared") + xlab("Number of freshwater sites") +
  scale_colour_manual(values=c("cyan", "red", "orange", "blue", "green", "gold", "purple"))

melt(m_quest) -> mmquest
colnames(mmquest) <- c("X1", "Phylum", "value")
p2 <- ggplot(mmquest, aes(x=X1, y=value)) + geom_point(data=mmquest, aes(col=Phylum)) + ylim(0,0.5) +
  ylab("% of marine nodes shared") + xlab("Number of marine sites") +
  scale_colour_manual(values=c("cyan", "red", "orange", "blue", "green", "gold", "purple"))

grid.arrange(p1, p2, ncol=2)

cbind(filter(mfquest, X1 == "44"), "freshwater") -> fw
cbind(filter(mmquest, X1 == "32"), "marine") -> mar
colnames(fw) <- c("X1", "phylum", "value", "habitat")
colnames(mar) <- c("X1","phylum", "value", "habitat")

rbind(fw, mar) -> per_table

# ##Create heatmap - removed from script
# phy_order <- c("Deltaproteobacteria", "Cyanobacteria", "Actinobacteria", 
#                "Bacteroidetes", "Betaproteobacteria", "Gammaproteobacteria",
#                "Alphaproteobacteria")
# per_table$phylum = factor(per_table$phylum,phy_order) #re-order phyla
# #colnames(per_table) <- c("phylum", "FW", "M")
# #meta_table.m <- melt(meta_table) #re-organize dataframe
# #rescale from 0 to 1 so that FW and Marine heatmaps match
# per_table <- ddply(per_table, .(habitat), transform, rescale = rescale(value))
# htmp <- ggplot(per_table, aes(habitat, phylum)) + 
#   geom_tile(aes(fill = value), colour = "white") + 
#   scale_fill_gradient(low = "white", high = "black")
# 
