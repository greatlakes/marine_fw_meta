##The main thing this one does is the UniFrac swap testing b ut it lso does the stuff from 2a I think?
##Also WARNING takes 5ever to run soz

##Adding in necessary libraries
library(phyloseq)
library(GUniFrac)
library(stringr)
library(ggplot2)
library(picante)
library(dplyr)
library(ggplot2)
library(ade4)
library(BioPhysConnectoR)

##Do you need to generate the p-values and unifrac distances for phyla?
generate_p_values<-'yes' #or 'no' for no

##Data input

directory<-'~/repos/marine_fw_meta/data/'
setwd(directory)
tree<-paste0(directory,'RAxML_result.mfmeta_MED_tree_test')
tree_input<-read.tree(file=tree)
tree_input<-phy_tree(tree_input)

#read in MED table
MED_bacteria_table = "~/repos/marine_fw_meta/data/MATRIX-COUNT_bacteria.txt"
t(read.table(MED_bacteria_table, header=TRUE, row.names = 1))-> MED_bacteria.otu

MED_archaea_table = "~/repos/marine_fw_meta/data/MATRIX-COUNT_archaea.txt"
t(read.table(MED_archaea_table, header=TRUE, row.names = 1))-> MED_archaea.otu

rownames(MED_bacteria.otu) -> nodenames_bacteria
rownames(MED_archaea.otu) -> nodenames_archaea

gsub("X0", "B0", nodenames_bacteria) -> nodenames_bacteria
gsub("X0", "A0", nodenames_archaea) -> nodenames_archaea

rownames(MED_bacteria.otu) <- nodenames_bacteria
rownames(MED_archaea.otu) <- nodenames_archaea

rownames(MED_bacteria.otu) -> rnames
data.frame(rnames, MED_bacteria.otu) -> MED_bacteria.df

rownames(MED_archaea.otu) -> rnames
data.frame(rnames, MED_archaea.otu) -> MED_archaea.df

bind_rows(MED_bacteria.df, MED_archaea.df) -> MED_combined.df

# convert NAs from samples with no archaea to 0s
MED_combined.df[is.na(MED_combined.df)] <- 0

rownames(MED_combined.df)<-MED_combined.df$rnames
MED_combined.df[,2:81]->MED_combined.otu

otu_table(MED_combined.otu, taxa_are_rows = TRUE) -> MED_phylo
nodenames<-rownames(MED_phylo)

#taxonomy files
bnode_tax_file = "~/repos/marine_fw_meta/data/NODE_REPRESENTATIVES_bacteria.nr_v128.wang.taxonomy_mod.txt"
anode_tax_file = "~/repos/marine_fw_meta/data/NODE_REPRESENTATIVES_archaea.nr_v128.wang.taxonomy_mod.txt"
#note:  the original taxonomy files from mothur were modified in excel by moving taxonomy to column C,
#using a "|" delimiter to separate OTU names and size; adding row one headers:  "OTU", "size", "taxonomy"

import_mothur(mothur_constaxonomy_file = bnode_tax_file) -> btax.tab
rownames(btax.tab) -> oligonames
cbind(btax.tab, oligonames) -> btax.tab2
tax_table(btax.tab2) -> btax.phylo

import_mothur(mothur_constaxonomy_file = anode_tax_file) -> atax.tab
rownames(atax.tab) -> oligonames
cbind(atax.tab, oligonames) -> atax.tab2
tax_table(atax.tab2) -> atax.phylo

##Reformatting tax.phylo names
bfillerfunction<-function(name){
  target_length<-10
  counter<-str_count(name)
  z<-target_length-(counter+1)
  os<-rep('0',z)
  os<-paste0(os,collapse='')
  result<-paste0('B',os,name)
  return(result)
}

rownames(btax.tab2)->btpnames
btpnewnames<-c(NA)
for(i in 1:length(btpnames)){
  bfillerfunction(btpnames[i])->btpnewnames[i]
}
btpnewnames->rownames(btax.phylo)

afillerfunction<-function(name){
  target_length<-10
  counter<-str_count(name)
  z<-target_length-(counter+1)
  os<-rep('0',z)
  os<-paste0(os,collapse='')
  result<-paste0('A',os,name)
  return(result)
}

rownames(atax.tab2)->atpnames
atpnewnames<-c(NA)
for(i in 1:length(atpnames)){
  afillerfunction(atpnames[i])->atpnewnames[i]
}
atpnewnames->rownames(atax.phylo)

rbind(btax.phylo, atax.phylo) -> merge.phylo
tax_table(merge.phylo) -> tax.phylo

##Changing Proteobacteria to the Proteobacterial Classes
proteos<-which(tax.phylo[,2]=='Proteobacteria')
tax.phylo[proteos,2]<-tax.phylo[proteos,3]

tree_input<-root(tree_input, outgroup="A000001667",resolve.root = TRUE)
sample.data<-paste0(directory,'sample_data.txt')
sample.data<-read.table(sample.data,header=TRUE,stringsAsFactors = FALSE)
sample_table<-sample_data(sample.data)

##Creating the actual phyloseq object and subsetting by phylum/proteobacterial class
final_input<-merge_phyloseq(tree_input,MED_phylo,tax.phylo,sample_table)

##This function performs the branch swapping test

statistic_getter<-function(master_phy){
  
  directory<-'~/repos/marine_fw_meta/data/'
  setwd(directory)
  tree<-paste0(directory,'RAxML_result.mfmeta_MED_tree_test')
  tree_input<-read.tree(file=tree)
  tree_input<-phy_tree(tree_input)
  
  #read in MED table
  MED_bacteria_table = "~/repos/marine_fw_meta/data/MATRIX-COUNT_bacteria.txt"
  t(read.table(MED_bacteria_table, header=TRUE, row.names = 1))-> MED_bacteria.otu
  
  MED_archaea_table = "~/repos/marine_fw_meta/data/MATRIX-COUNT_archaea.txt"
  t(read.table(MED_archaea_table, header=TRUE, row.names = 1))-> MED_archaea.otu
  
  rownames(MED_bacteria.otu) -> nodenames_bacteria
  rownames(MED_archaea.otu) -> nodenames_archaea
  
  gsub("X0", "B0", nodenames_bacteria) -> nodenames_bacteria
  gsub("X0", "A0", nodenames_archaea) -> nodenames_archaea
  
  rownames(MED_bacteria.otu) <- nodenames_bacteria
  rownames(MED_archaea.otu) <- nodenames_archaea
  
  rownames(MED_bacteria.otu) -> rnames
  data.frame(rnames, MED_bacteria.otu) -> MED_bacteria.df
  
  rownames(MED_archaea.otu) -> rnames
  data.frame(rnames, MED_archaea.otu) -> MED_archaea.df
  
  bind_rows(MED_bacteria.df, MED_archaea.df) -> MED_combined.df
  
  # convert NAs from samples with no archaea to 0s
  MED_combined.df[is.na(MED_combined.df)] <- 0
  
  rownames(MED_combined.df)<-MED_combined.df$rnames
  MED_combined.df[,2:81]->MED_combined.otu
  
  otu_table(MED_combined.otu, taxa_are_rows = TRUE) -> MED_phylo
  nodenames<-rownames(MED_phylo)
  
  #taxonomy files
  bnode_tax_file = "~/repos/marine_fw_meta/data/NODE_REPRESENTATIVES_bacteria.nr_v128.wang.taxonomy_mod.txt"
  anode_tax_file = "~/repos/marine_fw_meta/data/NODE_REPRESENTATIVES_archaea.nr_v128.wang.taxonomy_mod.txt"
  #note:  the original taxonomy files from mothur were modified in excel by moving taxonomy to column C,
  #using a "|" delimiter to separate OTU names and size; adding row one headers:  "OTU", "size", "taxonomy"
  
  import_mothur(mothur_constaxonomy_file = bnode_tax_file) -> btax.tab
  rownames(btax.tab) -> oligonames
  cbind(btax.tab, oligonames) -> btax.tab2
  tax_table(btax.tab2) -> btax.phylo
  
  import_mothur(mothur_constaxonomy_file = anode_tax_file) -> atax.tab
  rownames(atax.tab) -> oligonames
  cbind(atax.tab, oligonames) -> atax.tab2
  tax_table(atax.tab2) -> atax.phylo
  
  ##Reformatting tax.phylo names
  bfillerfunction<-function(name){
    target_length<-10
    counter<-str_count(name)
    z<-target_length-(counter+1)
    os<-rep('0',z)
    os<-paste0(os,collapse='')
    result<-paste0('B',os,name)
    return(result)
  }
  
  rownames(btax.tab2)->btpnames
  btpnewnames<-c(NA)
  for(i in 1:length(btpnames)){
    bfillerfunction(btpnames[i])->btpnewnames[i]
  }
  btpnewnames->rownames(btax.phylo)
  
  afillerfunction<-function(name){
    target_length<-10
    counter<-str_count(name)
    z<-target_length-(counter+1)
    os<-rep('0',z)
    os<-paste0(os,collapse='')
    result<-paste0('A',os,name)
    return(result)
  }
  
  rownames(atax.tab2)->atpnames
  atpnewnames<-c(NA)
  for(i in 1:length(atpnames)){
    afillerfunction(atpnames[i])->atpnewnames[i]
  }
  atpnewnames->rownames(atax.phylo)
  
  rbind(btax.phylo, atax.phylo) -> merge.phylo
  tax_table(merge.phylo) -> tax.phylo
  
  ##Changing Proteobacteria to the Proteobacterial Classes
  proteos<-which(tax.phylo[,2]=='Proteobacteria')
  tax.phylo[proteos,2]<-tax.phylo[proteos,3]
  
  # ##Parsing tip labels on tree for taxon and number
  # ##Reformatting sample number to match abundance matrix
  # taxa<-c(NA)
  # for(i in 1:length(tree_input$tip.label)){
  #   a<-strsplit(tree_input$tip.label[i],split = '\\|')
  #   taxa[i]<-a[[1]][2]
  #   tree_input$tip.label[i]<-paste0('X',a[[1]][1])
  # }
  # 
  # ##Reformatting tax.phylo names
  # fillerfunction<-function(name){
  #   target_length<-10
  #   counter<-str_count(name)
  #   z<-target_length-(counter+1)
  #   os<-rep('0',z)
  #   os<-paste0(os,collapse='')
  #   result<-paste0('X',os,name)
  #   return(result)
  # }
  # 
  # rownames(tax.phylo)->tpnames
  # tpnewnames<-c(NA)
  # for(i in 1:length(tpnames)){
  #   fillerfunction(tpnames[i])->tpnewnames[i]
  # }
  # tpnewnames->rownames(tax.phylo)
  tree_input<-root(tree_input, outgroup="A000001667",resolve.root = TRUE)
  sample.data<-paste0(directory,'sample_data.txt')
  sample.data<-read.table(sample.data,header=TRUE,stringsAsFactors = FALSE)
  sample_table<-sample_data(sample.data)
  
  ##Creating the actual phyloseq object and subsetting by phylum/proteobacterial class
  final_input<-merge_phyloseq(tree_input,MED_phylo,tax.phylo,sample_table)

  ##Determine which samples are fresh and which are marine
  fresh_samples<-rownames(sample_data(final_input)[
    which(sample_data(final_input)$habitat1=='freshwater'),
    ])
  marine_samples<-rownames(sample_data(final_input)[
    which(sample_data(final_input)$habitat1=='marine'),
    ])

  ##Determine which columns in the otu table refer to marine and fresh samples
  fresh_otus<-otu_table(final_input)[,
                                     which(colnames(otu_table(final_input))
                                           %in% fresh_samples)]
  marine_otus<-otu_table(final_input)[,
                                      which(colnames(otu_table(final_input))
                                            %in% marine_samples)]
  
  ##figure out which otu numbers align to phylum of interest
  nitros<-rownames(tax.phylo)[which(tax.phylo[,'Rank2']==master_phy)]
  if(length(nitros)<=10){
      stop('This taxonomic division has 10 or fewer MED nodes!')
  }
  
  ##determine if phylum is represented in 5 or more samples for freshwater and marine
  phylum_subset_f<-fresh_otus[nitros,]
  f_sums<-colSums(phylum_subset_f)
  f_length<-length(which(f_sums!=0))
  if(f_length<5){
    stop('This division is only represented in 4 or fewer freshwater samples!')
  }
  phylum_subset_m<-marine_otus[nitros,]
  m_sums<-colSums(phylum_subset_m)
  m_length<-length(which(m_sums!=0))
  if(m_length<5){
    stop('This division is only represented in 4 or fewer marine samples!')
  }
  
  ##Findign the total number of marine and freshwater appearances for the phylum
  phylum_sum_f<-rowSums(fresh_otus[nitros,])
  phylum_sum_m<-rowSums(marine_otus[nitros,])
  
  new_otus<-cbind(phylum_sum_f,phylum_sum_m)
  colnames(new_otus)<-c('Freshwater Pooled','Marine Pooled')
  new_otus<-otu_table(new_otus,taxa_are_rows = TRUE)
  
  new_phylo<-merge_phyloseq(new_otus,tax.phylo,tree_input)
  
  ##rarefy for even depth
  rarefied_phylo<-rarefy_even_depth(new_phylo,rngseed=8)
  
  ##Turn into presence absence data
  presence_absencer<-function(v){
    targets<-which(v!=0)
    v[targets]<-1
    return(v)
  }
  otu_table(rarefied_phylo)<-otu_table(apply(otu_table(rarefied_phylo),2,presence_absencer),taxa_are_rows = TRUE)
  test_list<-list(NA)
  test_list[[1]]<-otu_table(rarefied_phylo)
  
  ##Get random swaps of matrix
  set.seed(8)
  for(i in 2:1000){
    test_list[[i]]<-randomizeMatrix(otu_table(rarefied_phylo),null.model="independentswap",iterations=1000)
  }
  
  ##Calculate UniFrac Distances
  distance_wrapper<-function(x,tree){
    result<-GUniFrac(t(x),tree)$unifracs[,,'d_UW']
    result<-result[1,2]
    return(result)
  }
  final_distances<-lapply(test_list,distance_wrapper,tree=phy_tree(rarefied_phylo))
  final_distances<-do.call(c,final_distances)
  
  #Generating parameters for z tests
  avg<-mean(final_distances,na.rm=TRUE)
  std<-sd(final_distances,na.rm=TRUE)
  z_score<-(final_distances[1]-avg)/std
  statistic<-1-pnorm(z_score)
  return(c(final_distances[1],statistic))
}

##Getting abundance data
#directory<-'~/repos/marine_fw_meta/data/'
#taxa_input<-paste0(directory,'MED_nodes.nr_v128.wang.taxonomy_mod.txt')
#taxa_input<-import_mothur(mothur_constaxonomy_file = taxa_input)
#oligos<-rownames(taxa_input)
#taxa_input<-cbind(taxa_input,oligos)
#tax.phylo<-tax_table(taxa_input)
#proteos<-which(tax.phylo[,2]=='Proteobacteria')
#tax.phylo[proteos,2]<-tax.phylo[proteos,3]
phyla<-as.character(unique(tax.phylo[,'Rank2']))
output_list<-list(NA)
if(generate_p_values=='yes'){
  for(i in c(1:8,10:12,15,22,24,48,50)){
    output_list[[i]]<-statistic_getter(phyla[i])
  }
  ##,10,12:14,18,20,25:27
  ##phylum 9 (Chlorobi) is in 4 or fewer marine samples
  ##phylum 13 (Nitrospirae?) has 10 or fewer nodes
  ##phylum 49 (archaea_unclassified) has 10 or fewer nodes
  ##phylum 14 (marinimicrobia sar 406) is in 4 or fewer freshwater samples
  ##phylum 16 (nitrospinae) is in 4 or fewer freshwater samples
  ##phylum 17 (Armatimonadetes) is in 4 or fewer marine samples
  ##phylum 20 (Ignavibacteriae) has 10 or fewer MED nodes
  ##phylum 19 (Lentisphaerae) has 10 or fewer MED nodes
  ##phylum 27 (Caldiserica) has 10 or fewer MED nodes
  ##phylum 23 (SBR1093) has 10 or fewer MED nodes
  ##phylum 32 (AEGEAN-245) has 10 or fewer MED nodes
  ##phylum 51 (Woesarchaeota_(DHVEG-6)) has 10 or fewer MED nodes
  ##phylum 25 (Atribacteria) has 10 or fewer MED nodes
  ##phylum 28 (Proteobacteria_Incertae_Sedis) has 10 or fewer MED nodes
  ##phylum 26 (Deinococcus-Thermus) has 10 or fewer MED nodes
  ##phylum 29 (Parcubacteria) in 4 or fewer marine samples
  ##phylum 31 (Spirochaetae) is in 4 or feer marine samples
  ##phylum 30 (Fusobacteria) in 4 or fewer marine samples
  ##phylum 36 (Omnitrophica) in 4 or fewer marine samples
  ##phylum 33 (Chlamydiae) has 10 or fewer MED nodes
  ##phylum 45 (Unknown-unclassified) has 10 or fewer MED nodes (yay)
  ##phylum 37 (PAUC34f) has 10 or fewer MED nodes
  ##phylum 39 (Cloacimonetes) has 10 or fewer MED nodes
  ##phylum 35 (TM6_(Dependentiae) in 4 or fewer marine samples
  ##phylum 38 (Tenericutes) has 10 or fewer MED nodes
  ##phylum 42 (Fibrobacteres) in 4 or fewer marine samples
  ##phylum 40 (Elusimicrobia) in 4 or fewer marine samples
  ##phylum 44 (WS1) has 10 or fewer MED nodes
  ##phylum 43 (SR1_(Absconditabacteria)) has 10 or fewer MED nodes
  ##phylum 46 (Microgenomates) has 10 or fewer MED nodes
  ######phylum 49 (Synergistetes) has 10 or fewer MED nodes
  ##phylum 47 (SPOTSOCT00m83) has 10 or fewer MED nodes
  ##phylum 41 (Gracilibacteria) has 10 or fewer MED nodes
  #######phylum 52 (Saccharibacteria) has 10 or fewer MED nodes
  #######phylum 53 (Aminicenantes) has 10 or fewer MED nodes
  #######phylum 54 (Poribacteria) has 10 or fewer MED nodes
  #######phylum 55 (Hydrogenedentes) has 10 or fewer MED nodes
  #######phylum 56 (FBP) has 10 or fewer MED nodes
  #######phylum 57 (BRC1) has 10 or fewer MED nodes
  ##phylum 34 (Latescibacteria) has 10 or fewer MED nodes
  #######phylum 59 (Nanohaloarchaeota) has 10 or fewer MED nodes
  #######phylum 60 (JTB23) has 10 or fewer MED nodes
  #######phylum 61 (Aenigmarchaeota) has 10 or fewer MED nodes
  #######phylum 62 (Lokiarchaeota) has 10 or fewer MED nodes
  #######phylum 63 (Candidatus_Berkelbacteria) has 10 or fewer MED nodes
  #######phylum 64 (Peregrinibacteria) has 10 or fewer MED nodes
  #######phylum 65 (Tectomicrobia) has 10 or fewer MED nodes

  null_entries<-lapply(output_list,is.null)
  null_entries<-do.call(c,null_entries)
  final_phyla<-phyla[which(null_entries==FALSE)]
  final_stats<-output_list[c(which(null_entries==FALSE))]
  final_stats<-do.call(rbind,final_stats)
  final_stats<-cbind(phyla[c(1:8,10:12,15,22,24,48,50)],final_stats)
  final_stats<-data.frame(phylum=final_stats[,1],
                          unifrac_distance=final_stats[,2],
                          p_value=final_stats[,3])
  ##Saving p value results
  write.table(final_stats,'~/repos/marine_fw_meta/results/figure3pvalues_test')
}

# ##Setting the directory and reading in phyloseq files
# directory<-'~/repos/marine_fw_meta/data/'
# setwd(directory)
# tree<-paste0(directory,'RAxML_result.mfmeta_MED_tree_test')
# tree_input<-read.tree(file=tree)
# tree_input<-phy_tree(tree_input)
# med_phylo_input<-paste0(directory,'MED.count_table')
# med_phylo_input<-read.table(med_phylo_input,header=TRUE,row.names=1)
# MED_phylo<-otu_table(med_phylo_input,taxa_are_rows = TRUE)
# nodenames<-rownames(MED_phylo)
# taxa_input<-paste0(directory,'MED_nodes.nr_v128.wang.taxonomy_mod.txt')
# taxa_input<-import_mothur(mothur_constaxonomy_file = taxa_input)
# oligos<-rownames(taxa_input)
# taxa_input<-cbind(taxa_input,oligos)
# tax.phylo<-tax_table(taxa_input)
# proteos<-which(tax.phylo[,2]=='Proteobacteria')
# tax.phylo[proteos,2]<-tax.phylo[proteos,3]
# ##Parsing tip labels on tree for taxon and number
# ##Reformatting sample number to match abundance matrix
# taxa<-c(NA)
# for(i in 1:length(tree_input$tip.label)){
#   a<-strsplit(tree_input$tip.label[i],split = '\\|')
#   taxa[i]<-a[[1]][2]
#   tree_input$tip.label[i]<-paste0('X',a[[1]][1])
# }
# 
# ##Reformatting tax.phylo names
# library(stringr)
# fillerfunction<-function(name){
#   target_length<-10
#   counter<-str_count(name)
#   z<-target_length-(counter+1)
#   os<-rep('0',z)
#   os<-paste0(os,collapse='')
#   result<-paste0('X',os,name)
#   return(result)
# }
# 
# rownames(tax.phylo)->tpnames
# tpnewnames<-c(NA)
# for(i in 1:length(tpnames)){
#   fillerfunction(tpnames[i])->tpnewnames[i]
# }
# tpnewnames->rownames(tax.phylo)
# tree_input<-root(tree_input, outgroup="X000010935",resolve.root = TRUE)
# sample.data<-paste0(directory,'sample_data.txt')
# sample.data<-read.table(sample.data,header=TRUE,stringsAsFactors = FALSE)
# sample_table<-sample_data(sample.data)
# final_input<-merge_phyloseq(tree_input,MED_phylo,tax.phylo,sample_table)


phyla_list<-list(NA)
phyla<-as.character(unique(tax_table(final_input)[,2]))
for(i in 1:length(unique(tax_table(final_input)[,2]))){
  phyla_list[[i]]<-subset_taxa(final_input,Rank2==phyla[i])
}
phylum_selecter<-function(phylo){
  result<-nrow(otu_table(phylo))
  return(result)
}
##Trimming down only phyla w/10 or more nodes
node_counts<-lapply(phyla_list,phylum_selecter)
node_counts<-do.call(rbind,node_counts)
keepers<-which(node_counts[,1]>=10)
new_phyla_list<-phyla_list[c(as.numeric(keepers))]
new_phyla<-phyla[as.numeric(keepers)]
##Trimming down phyla with 500 or more total sequences across samples
enough_samples<-function(phylo){
  result<-length(which(sample_sums(phylo)>=500))
  return(result)
}
enough_sequences<-lapply(new_phyla_list,enough_samples)
enough_sequences<-do.call(rbind,enough_sequences)
keepers<-which(enough_sequences[,1]>1)
final_phyla_list<-new_phyla_list[c(keepers)]
final_phyla<-new_phyla[keepers]
##Pruning trees and rarefying to even depth
pruner<-function(tree){
  prune<-prune_samples(names(which(sample_sums(tree)>=500)),tree)
  even<-rarefy_even_depth(prune,rngseed = 711)
  return(even)
}
pruned<-lapply(final_phyla_list,pruner)
pruned_trees<-lapply(pruned,phy_tree)
lapply(pruned_trees,is.rooted)
final_otu_tables<-lapply(pruned,otu_table)
final_otu_tables<-lapply(final_otu_tables,t)
final_sample_data<-lapply(pruned, function(x) sample_data(x)$habitat1)
unifracs<-list(NA)
##Calculating unifrac distnaces
for(i in 1:length(pruned)){
  unifracs[[i]]<-GUniFrac(otu.tab = final_otu_tables[[i]],
                          tree=pruned_trees[[i]])$unifracs
  unifracs[[i]]<-unifracs[[i]][,,'d_UW']
}
##Summarizing unifrac data for each otu among samples
plottingprepfunctiona<-function(sampledata,distances){
  fresh<-which(sampledata=='freshwater')
  marine<-which(sampledata=='marine')
  fm<-distances[fresh,marine]
  fm<-c(fm)
  fm_sum<-quantile(fm,c(0,0.5,1))
  result<-data.frame(q1=as.numeric(fm_sum[1]),
                     meds=as.numeric(fm_sum[2]),
                     q3=as.numeric(fm_sum[3]),
                     n_fresh=length(fresh),n_mar=length(marine))
  return(result)
}
frame_list<-list(NA)
for(i in 1:length(unifracs)){
  frame_list[[i]]<-plottingprepfunctiona(final_sample_data[[i]],unifracs[[i]])
}
frame_list<-do.call(rbind,frame_list)
frame_list<-data.frame(phylum=final_phyla,
                       n_fresh=frame_list$n_fresh,
                       n_marine=frame_list$n_mar,
                       q1=frame_list$q1,
                       med=frame_list$meds,
                       q3=frame_list$q3)
##Reading in pooled data
mf_pool<-read.table('~/repos/marine_fw_meta/results/figure3pvalues',stringsAsFactors = FALSE)
frame_list$phylum<-as.character(frame_list$phylum)
frame_list<-mat.sort(frame_list,'phylum')
mf_pool<-mat.sort(mf_pool,'phylum')
plotting_keepers<-which(frame_list$n_fresh>=3 & frame_list$n_marine>=3)
plotting_keeper_phyla<-as.character(frame_list$phylum[plotting_keepers])
mf_pool_subset<-mf_pool[which(mf_pool$phylum %in% plotting_keeper_phyla),]
updated_keeper_phyla<-mf_pool_subset$phylum
##Making sure to match pooled data to summarized sample data
plotting_frame<-data.frame(phylum=updated_keeper_phyla,
                           mf_pool=mf_pool_subset$unifrac_distance,
                           mf_sample=frame_list$med[which(frame_list$phylum %in% updated_keeper_phyla)],
                           mf_q1=frame_list$q1[which(frame_list$phylum %in% updated_keeper_phyla)],
                           mf_q3=frame_list$q3[which(frame_list$phylum %in% updated_keeper_phyla)],
                           p_values=mf_pool_subset$p_value)
signif<-which(plotting_frame$p_values<=0.05/nrow(mf_pool))
insignif<-setdiff(1:nrow(plotting_frame),signif)
plotting_frame$p_values[signif]<-'significant'
plotting_frame$p_values[insignif]<-'not significant'
##Plotting and saving result
plot<-ggplot(data=plotting_frame,aes(x=mf_pool,y=mf_sample,col=phylum,shape=p_values))+geom_point()+
  geom_errorbar(aes(ymax=mf_q3,ymin=mf_q1))+xlim(0,1)+ylim(0,1)+geom_abline(slope=1,intercept=0)
ggsave(filename = 'figure2a',device = 'eps',path = '~/repos/marine_fw_meta/results')

family_getter<-function(master_phy,division){
  directory<-'~/repos/marine_fw_meta/data/'
  treename<-'marine_fw4.fasttree'
  tree<-read.tree(paste0(directory,treename))
  tree<-phy_tree(tree)
  med_phylo_input<-paste0(directory,'MED.count_table')
  med_phylo_input<-read.table(med_phylo_input,header=TRUE,row.names=1)
  MED_phylo<-otu_table(med_phylo_input,taxa_are_rows = TRUE)
  nodenames<-rownames(MED_phylo)
  taxa_input<-paste0(directory,'MED_nodes.nr_v128.wang.taxonomy_mod.txt')
  taxa_input<-import_mothur(mothur_constaxonomy_file = taxa_input)
  oligos<-rownames(taxa_input)
  taxa_input<-cbind(taxa_input,oligos)
  tax.phylo<-tax_table(taxa_input)
  proteos<-which(tax.phylo[,2]=='Proteobacteria')
  tax.phylo[proteos,2]<-tax.phylo[proteos,3]
  ##Parsing tip labels on tree for taxon and number
  ##Reformatting sample number to match abundance matrix
  taxa<-c(NA)
  for(i in 1:length(tree$tip.label)){
    a<-strsplit(tree$tip.label[i],split = '\\|')
    taxa[i]<-a[[1]][2]
    tree$tip.label[i]<-paste0('X',a[[1]][1])
  }
  
  ##Reformatting tax.phylo names
  fillerfunction<-function(name){
    target_length<-10
    counter<-str_count(name)
    z<-target_length-(counter+1)
    os<-rep('0',z)
    os<-paste0(os,collapse='')
    result<-paste0('X',os,name)
    return(result)
  }
  
  rownames(tax.phylo)->tpnames
  tpnewnames<-c(NA)
  for(i in 1:length(tpnames)){
    fillerfunction(tpnames[i])->tpnewnames[i]
  }
  tpnewnames->rownames(tax.phylo)
  tree<-root(tree, outgroup="X000010935",resolve.root = TRUE)
  sample.data<-paste0(directory,'sample_data.txt')
  sample.data<-read.table(sample.data,header=TRUE,stringsAsFactors = FALSE)
  sample_table<-sample_data(sample.data)
  final_input<-merge_phyloseq(tree,MED_phylo,sample_table,tax.phylo)
  
  ##Determine which samples are fresh and which are marine
  fresh_samples<-rownames(sample_data(final_input)[
    which(sample_data(final_input)$habitat1=='freshwater'),
    ])
  marine_samples<-rownames(sample_data(final_input)[
    which(sample_data(final_input)$habitat1=='marine'),
    ])
  
  ##Determine which columns in the otu table refer to marine and fresh samples
  fresh_otus<-otu_table(final_input)[,
                                     which(colnames(otu_table(final_input))
                                           %in% fresh_samples)]
  marine_otus<-otu_table(final_input)[,
                                      which(colnames(otu_table(final_input))
                                            %in% marine_samples)]
  
  ##figure out which otu numbers align to phylum of interest
  nitros<-rownames(tax.phylo)[which(tax.phylo[,'Rank2']==master_phy)]
  families<-as.character(unique(tax.phylo[nitros,division]))
  family_list<-list(NA)
  for(i in 1:length(families)){
    family_list[[i]]<-rownames(tax.phylo)[which(tax.phylo[,division]==families[i])]
  }
  num_nodes<-lapply(family_list,length)
  num_nodes<-do.call(c,num_nodes)
  enough_nodes<-which(num_nodes>=5)
  if(length(enough_nodes)==0){
    stop('No families in this phylum have more than 5 MED nodes!')
  }
  family_list<-family_list[c(enough_nodes)]
  families<-families[enough_nodes]
  ##determine if phylum is represented in 5 or more samples for freshwater and marine
  phylum_subset_f<-lapply(family_list,function(x) fresh_otus[x,])
  f_sums<-lapply(phylum_subset_f,colSums)
  f_length<-lapply(f_sums,function(x) length(which(x!=0)))
  f_length<-do.call(c,f_length)
  f_long_enough<-which(f_length>=3)
  if(length(f_long_enough)==0){
    stop('None of the families in this phylum are present in more than 2 freshwater samples')
  }
  family_list<-family_list[c(f_long_enough)]
  families<-families[f_long_enough]
  phylum_subset_m<-lapply(family_list,function(x) marine_otus[x,])
  m_sums<-lapply(phylum_subset_m,colSums)
  m_length<-lapply(m_sums,function(x) length(which(x!=0)))
  m_length<-do.call(c,m_length)
  m_long_enough<-which(m_length>=3)
  if(length(m_long_enough)==0){
    stop('None of the families in this phylum are present in more than 2 marine samples')
  }
  family_list<-family_list[c(m_long_enough)]
  families<-families[m_long_enough]
  phylum_sum_f<-lapply(family_list,function(x) rowSums(fresh_otus[x,]))
  phylum_sum_m<-lapply(family_list,function(x) rowSums(marine_otus[x,]))
  new_otus<-list(NA)
  new_phylo<-list(NA)
  for(i in 1:length(phylum_sum_m)){
    new_otus[[i]]<-cbind(phylum_sum_f[[i]],phylum_sum_m[[i]])
    colnames(new_otus[[i]])<-c('Freshwater Pooled','Marine Pooled')
    new_otus[[i]]<-otu_table(new_otus[[i]],taxa_are_rows = TRUE)
    new_phylo[[i]]<-merge_phyloseq(new_otus[[i]],tax.phylo,tree)
  }
  
  
  ##rarefy for even depth
  rarefied_phylo<-lapply(new_phylo,rarefy_even_depth,rngseed=8)
  
  presence_absencer<-function(v){
    targets<-which(v!=0)
    v[targets]<-1
    return(v)
  }
  for(i in 1:length(rarefied_phylo)){
    otu_table(rarefied_phylo[[i]])<-otu_table(apply(otu_table(rarefied_phylo[[i]]),2,presence_absencer),taxa_are_rows = TRUE)
  }
  distance_wrapper<-function(x,tree){
    result<-GUniFrac(t(x),tree)$unifracs[,,'d_UW']
    result<-result[1,2]
    return(result)
  }
  final_distance<-lapply(rarefied_phylo,function(x) distance_wrapper(otu_table(x),phy_tree(x)))
  final_distance<-do.call(c,final_distance)
  result<-cbind(families,final_distance)
  return(result)
}

phyla_to_family_get<-c('Actinobacteria','Alphaproteobacteria','Bacteroidetes','Betaproteobacteria',
                       'Chloroflexi','Cyanobacteria','Deltaproteobacteria','Gammaproteobacteria',
                       'Planctomycetes','Verrucomicrobia','Acidobacteria',
                       'Epsilonproteobacteria','Firmicutes',
                       'Gemmatimonadetes','Nitrospirae','Thaumarchaeota')
family_answers<-list(NA)
for(i in 1:length(phyla_to_family_get)){
  family_answers[[i]]<-family_getter(phyla_to_family_get[i],division='Rank5')
}
phylum_counter<-lapply(family_answers,nrow)
phylum_counter<-do.call(c,phylum_counter)
phylum_col<-rep(phyla_to_family_get[1],each=phylum_counter[1])
for(i in 2:length(family_answers)){
  phylum_col<-c(phylum_col,rep(phyla_to_family_get[i],phylum_counter[i]))
}
family_answers<-do.call(rbind,family_answers)
family_plot_frame<-data.frame(phylum=phylum_col,family=family_answers[,1],distance=as.numeric(as.character(family_answers[,2])))
fam_plot<-ggplot(family_plot_frame,aes(x=distance,fill=phylum))+geom_dotplot(stackgroups=TRUE,binpositions='all',binwidth = 0.05,dotsize=0.75)
ggsave('figure2b',fam_plot,device="eps",path="~/repos/marine_fw_meta/results")
write.table(family_plot_frame,'~/repos/marine_fw_meta/results/family_unifrac_plot')
order_answers<-list(NA)
for(i in 1:length(phyla_to_family_get)){
  order_answers[[i]]<-family_getter(phyla_to_family_get[i],division='Rank4')
}
phylum_counter_order<-lapply(order_answers,nrow)
phylum_counter_order<-do.call(c,phylum_counter_order)
phylum_col_order<-rep(phyla_to_family_get[1],phylum_counter_order[1])
for(i in 2:length(order_answers)){
  phylum_col_order<-c(phylum_col_order,rep(phyla_to_family_get[i],phylum_counter_order[i]))
}
order_answers<-do.call(rbind,order_answers)
order_plot_frame<-data.frame(phylum=phylum_col_order,order=order_answers[,1],distance=as.numeric(as.character(order_answers[,2])))
order_plot<-ggplot(order_plot_frame,aes(x=distance,fill=phylum))+geom_dotplot(stackgroups=TRUE,binpositions='all',binwidth = 0.05,dotsize=0.75)
ggsave('figure2c',order_plot,device='eps',path='~/repos/marine_fw_meta/results')
write.table(order_plot_frame,'~/repos/marine_fw_meta/results/order_unifrac_plot')
