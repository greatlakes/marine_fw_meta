Marine-Freshwater Meta-analysis
Contributors:  S. Paver, D. Muratore

This repository contains the data* and code to reproduce figures in
Re-evaluating the salty divide:  phylogenetic specificity of transitions 
between marine and freshwater systems

Data files too large to be stored on bitbucket can be accessed at:
https://uchicago.box.com/s/o4vjbe1wmf0umj87jw5bhs2w0y6dtdvq
