TREE_COLORS
#use this template to define branch colors and styles, colored ranges and label colors/font styles
#lines starting with a hash are comments and ignored during parsing

#=================================================================#
#                    MANDATORY SETTINGS                           #
#=================================================================#
#select the separator which is used to delimit the data below (TAB,SPACE or COMMA).This separator must be used throughout this file (except in the SEPARATOR line, which uses space).

SEPARATOR TAB
#SEPARATOR SPACE
#SEPARATOR COMMA

#First 3 fields define the node, type and color
#Possible types are:
#'range': defines a colored range (colored background for labels/clade)
#'clade': defines color/style for all branches in a clade
#'branch': defines color/style for a single branch
#'label': defines font color/style for the leaf label

#The following additional fields are required:
#for 'range', field 4 defines the colored range label (used in the legend)

#The following additional fields are optional:
#for 'label', field 4 defines the font style ('normal',''bold', 'italic' or 'bold-italic') and field 5 defines the numeric scale factor for the font size (eg. with value 2, font size for that label will be 2x the standard size)
#for 'clade' and 'branch', field 4 defines the branch style ('normal' or 'dashed') and field 5 defines the branch width scale factor (eg. with value 0.5, branch width for that clade will be 0.5 the standard width)

#Internal tree nodes can be specified using IDs directly, or using the 'last common ancestor' method described in iTOL help pages
#=================================================================#
#       Actual data follows after the DATA keyword              #
#=================================================================#
DATA
#NODE_ID,TYPE,COLOR,LABEL_OR_STYLE,SIZE_FACTOR

#Examples
#internal node with solid branches colored blue and twice the standard width
#9031|9606,clade,#0000ff,normal,2
#internal node with dashed branches colored red and one half the standard width
#601|340,clade,#ff0000,dashed,0.5
#a single internal branch colored green, dashed and 5 times the normal width
#915|777,branch,#00ff00,dashed,5

#colored range covering all leaves of an internal node, colored red and with label 'Eukaryota'
#184922|9606,range,#ff0000,Eukaryota
#examples of colored ranges from iTOL's Tree of Life
#2190|2287,range,#aaffaa,Archaea
#623|1502,range,#aaaaff,Bacteria

#leaf label for node 9606 will be displayed in green, bold and twice the regular font size
#9606,label,#00ff00,bold,2

#leaf label for node 9031 will be displayed in yellow, bold italic and half the regular font size
#9031,label,#ffff00,bold-italic,0.5

#leaf label for node 8015 will be displayed in blue
#8015,label,#0000ff

B000008512	range	#ffffff	NA
B000014228	range	#ffffff	Planktothrix
B000014227	range	#ffffff	Planktothrix
B000008662	range	#ffffff	Anabaena
B000008664	range	#ffffff	NA
B000000774	range	#ffffff	FamilyI_ge
B000003463	range	#ffffff	Snowella
B000005285	range	#ffffff	NA
B000021923	range	#ffffff	Chloroplast_ge
B000020963	range	#ffffff	NA
B000005289	range	#ffffff	Caenarcaniphilales_ge
B000000932	range	#ffffff	Trichodesmium
B000014725	range	#ffffff	NA
B000031028	range	#ffffff	NA
B000031027	range	#ffffff	NA
B000014433	range	#ffffff	NA
B000014221	range	#ffffff	NA
B000004366	range	#ffffff	NA
B000004346	range	#ffffff	NA
B000004356	range	#ffffff	NA
B000014376	range	#d80d46	Synechococcus
B000014872	range	#d80d46	Synechococcus
B000026402	range	#d80d46	Synechococcus
B000026401	range	#d80d46	Synechococcus
B000014871	range	#d80d46	Synechococcus
B000004122	range	#d80d46	Synechococcus
B000014267	range	#d80d46	Synechococcus
B000026090	range	#ffffff	NA
B000026089	range	#d80d46	Synechococcus
B000014868	range	#d80d46	Synechococcus
B000000776	range	#d80d46	Synechococcus
B000014266	range	#d80d46	Synechococcus
B000026069	range	#d80d46	Synechococcus
B000026066	range	#d80d46	Synechococcus
B000014270	range	#d80d46	Synechococcus
B000026097	range	#d80d46	Synechococcus
B000026098	range	#d80d46	Synechococcus
B000004148	range	#d80d46	Synechococcus
B000014268	range	#d80d46	Synechococcus
B000026096	range	#d80d46	Synechococcus
B000014353	range	#ffffff	NA
B000026117	range	#d80d46	Synechococcus
B000014468	range	#ffffff	NA
B000014470	range	#ffffff	NA
B000014445	range	#d80d46	Synechococcus
B000026116	range	#ffffff	NA
B000014400	range	#0dc632	Prochlorococcus
B000026140	range	#0dc632	Prochlorococcus
B000026139	range	#0dc632	Prochlorococcus
B000014404	range	#ffffff	NA
B000014403	range	#0dc632	Prochlorococcus
B000004120	range	#0dc632	Prochlorococcus
B000026128	range	#ffffff	NA
B000008471	range	#0dc632	Prochlorococcus
B000008591	range	#0dc632	Prochlorococcus
B000008569	range	#0dc632	Prochlorococcus
B000008425	range	#0dc632	Prochlorococcus
B000008470	range	#0dc632	Prochlorococcus
B000000813	range	#0dc632	Prochlorococcus
B000000783	range	#0dc632	Prochlorococcus
B000008525	range	#0dc632	Prochlorococcus
B000008548	range	#0dc632	Prochlorococcus
B000008571	range	#0dc632	Prochlorococcus
B000008593	range	#0dc632	Prochlorococcus
B000000795	range	#0dc632	Prochlorococcus
B000022527	range	#0dc632	Prochlorococcus
B000022526	range	#0dc632	Prochlorococcus
B000022522	range	#0dc632	Prochlorococcus
B000008526	range	#0dc632	Prochlorococcus
B000008551	range	#0dc632	Prochlorococcus
B000022519	range	#0dc632	Prochlorococcus
B000008550	range	#0dc632	Prochlorococcus
B000008457	range	#0dc632	Prochlorococcus
B000022512	range	#0dc632	Prochlorococcus
B000008510	range	#0dc632	Prochlorococcus
B000008509	range	#0dc632	Prochlorococcus
B000008483	range	#0dc632	Prochlorococcus
B000022509	range	#0dc632	Prochlorococcus
B000008485	range	#0dc632	Prochlorococcus
B000022508	range	#0dc632	Prochlorococcus
B000008366	range	#0dc632	Prochlorococcus
B000000801	range	#0dc632	Prochlorococcus
B000008389	range	#0dc632	Prochlorococcus
B000008401	range	#0dc632	Prochlorococcus
B000008390	range	#0dc632	Prochlorococcus
B000008490	range	#0dc632	Prochlorococcus
B000008559	range	#0dc632	Prochlorococcus
B000022502	range	#0dc632	Prochlorococcus
B000022503	range	#0dc632	Prochlorococcus
B000008393	range	#0dc632	Prochlorococcus
B000008492	range	#0dc632	Prochlorococcus
B000008362	range	#0dc632	Prochlorococcus
B000008363	range	#0dc632	Prochlorococcus
B000008491	range	#0dc632	Prochlorococcus
B000008558	range	#0dc632	Prochlorococcus
B000000775	range	#0dc632	Prochlorococcus
B000000802	range	#0dc632	Prochlorococcus
B000010550	range	#0dc632	Prochlorococcus
B000008495	range	#0dc632	Prochlorococcus
B000000791	range	#0dc632	Prochlorococcus
B000000106	range	#0dc632	Prochlorococcus
B000004128	range	#0dc632	Prochlorococcus
B000010551	range	#0dc632	Prochlorococcus
B000010554	range	#0dc632	Prochlorococcus
B000008388	range	#0dc632	Prochlorococcus
B000000900	range	#0dc632	Prochlorococcus
B000008398	range	#0dc632	Prochlorococcus
B000008608	range	#0dc632	Prochlorococcus
B000008607	range	#0dc632	Prochlorococcus
B000008391	range	#0dc632	Prochlorococcus
B000008396	range	#0dc632	Prochlorococcus
B000008496	range	#0dc632	Prochlorococcus
B000008392	range	#0dc632	Prochlorococcus
B000000191	range	#0dc632	Prochlorococcus
B000008367	range	#0dc632	Prochlorococcus
B000022511	range	#0dc632	Prochlorococcus
B000003465	range	#ffffff	NA
B000022520	range	#0dc632	Prochlorococcus
B000008575	range	#0dc632	Prochlorococcus
B000000784	range	#0dc632	Prochlorococcus
B000008426	range	#0dc632	Prochlorococcus
B000008422	range	#0dc632	Prochlorococcus
B000008423	range	#0dc632	Prochlorococcus
B000008456	range	#0dc632	Prochlorococcus
B000008527	range	#0dc632	Prochlorococcus
B000008528	range	#0dc632	Prochlorococcus
B000008572	range	#0dc632	Prochlorococcus
B000000781	range	#0dc632	Prochlorococcus
B000014358	range	#0dc632	Prochlorococcus
B000026095	range	#d80d46	Synechococcus
B000026084	range	#ffffff	NA
B000026094	range	#d80d46	Synechococcus
B000026083	range	#ffffff	NA
B000026082	range	#ffffff	NA
B000014248	range	#ffffff	NA
B000014247	range	#ffffff	NA
B000004102	range	#0dc632	Prochlorococcus
B000014240	range	#ffffff	NA
B000014241	range	#ffffff	NA
B000008458	range	#0dc632	Prochlorococcus
B000014242	range	#ffffff	NA
B000014243	range	#ffffff	NA
B000026130	range	#ffffff	NA
B000004119	range	#ffffff	NA
B000026131	range	#ffffff	NA
B000008438	range	#d80d46	Synechococcus
B000008441	range	#d80d46	Synechococcus
B000014348	range	#d80d46	Synechococcus
B000014480	range	#ffffff	NA
B000014479	range	#d80d46	Synechococcus
B000014349	range	#d80d46	Synechococcus
B000014333	range	#d80d46	Synechococcus
B000014332	range	#d80d46	Synechococcus
B000014335	range	#d80d46	Synechococcus
B000014773	range	#d80d46	Synechococcus
B000014771	range	#d80d46	Synechococcus
B000014774	range	#d80d46	Synechococcus
B000014772	range	#d80d46	Synechococcus
B000023479	range	#d80d46	Synechococcus
B000010533	range	#d80d46	Synechococcus
B000010624	range	#d80d46	Synechococcus
B000023480	range	#d80d46	Synechococcus
B000014323	range	#ffffff	NA
B000026389	range	#ffffff	NA
B000014222	range	#d80d46	Synechococcus
B000026388	range	#ffffff	NA
B000004124	range	#d80d46	Synechococcus
B000026341	range	#d80d46	Synechococcus
B000026342	range	#ffffff	NA
B000010532	range	#d80d46	Synechococcus
B000023494	range	#d80d46	Synechococcus
B000023493	range	#d80d46	Synechococcus
B000026183	range	#d80d46	Synechococcus
B000026196	range	#d80d46	Synechococcus
B000026191	range	#d80d46	Synechococcus
B000026195	range	#d80d46	Synechococcus
B000026199	range	#d80d46	Synechococcus
B000026174	range	#d80d46	Synechococcus
B000026175	range	#d80d46	Synechococcus
B000026154	range	#ffffff	NA
B000030999	range	#d80d46	Synechococcus
B000026153	range	#ffffff	NA
B000026192	range	#d80d46	Synechococcus
B000031003	range	#d80d46	Synechococcus
B000014430	range	#d80d46	Synechococcus
B000026187	range	#d80d46	Synechococcus
B000014451	range	#d80d46	Synechococcus
B000014834	range	#ffffff	NA
B000014852	range	#ffffff	NA
B000026366	range	#ffffff	NA
B000026353	range	#ffffff	NA
B000014825	range	#ffffff	NA
B000026364	range	#ffffff	NA
B000026379	range	#ffffff	NA
B000026356	range	#ffffff	Cyanobium
B000026355	range	#ffffff	NA
B000004341	range	#d80d46	Synechococcus
B000026284	range	#d80d46	Synechococcus
B000014718	range	#ffffff	NA
B000014720	range	#ffffff	NA
B000023998	range	#ffffff	Gastranaerophilales_ge
B000023997	range	#ffffff	Gastranaerophilales_ge
B000011631	range	#ffffff	Gastranaerophilales_ge
B000002736	range	#ffffff	Gastranaerophilales_ge
B000003529	range	#ffffff	Vampirovibrionales_ge
B000010405	range	#02f9f9	ML635J-21
B000010410	range	#02f9f9	ML635J-21
B000005896	range	#02f9f9	ML635J-21
B000010404	range	#02f9f9	ML635J-21
B000005893	range	#02f9f9	ML635J-21
B000016640	range	#02f9f9	ML635J-21
B000018346	range	#02f9f9	ML635J-21
B000010160	range	#02f9f9	ML635J-21
B000010162	range	#02f9f9	ML635J-21
