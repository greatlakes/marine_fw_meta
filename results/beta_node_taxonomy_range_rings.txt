TREE_COLORS
#use this template to define branch colors and styles, colored ranges and label colors/font styles
#lines starting with a hash are comments and ignored during parsing

#=================================================================#
#                    MANDATORY SETTINGS                           #
#=================================================================#
#select the separator which is used to delimit the data below (TAB,SPACE or COMMA).This separator must be used throughout this file (except in the SEPARATOR line, which uses space).

SEPARATOR TAB
#SEPARATOR SPACE
#SEPARATOR COMMA

#First 3 fields define the node, type and color
#Possible types are:
#'range': defines a colored range (colored background for labels/clade)
#'clade': defines color/style for all branches in a clade
#'branch': defines color/style for a single branch
#'label': defines font color/style for the leaf label

#The following additional fields are required:
#for 'range', field 4 defines the colored range label (used in the legend)

#The following additional fields are optional:
#for 'label', field 4 defines the font style ('normal',''bold', 'italic' or 'bold-italic') and field 5 defines the numeric scale factor for the font size (eg. with value 2, font size for that label will be 2x the standard size)
#for 'clade' and 'branch', field 4 defines the branch style ('normal' or 'dashed') and field 5 defines the branch width scale factor (eg. with value 0.5, branch width for that clade will be 0.5 the standard width)

#Internal tree nodes can be specified using IDs directly, or using the 'last common ancestor' method described in iTOL help pages
#=================================================================#
#       Actual data follows after the DATA keyword              #
#=================================================================#
DATA
#NODE_ID,TYPE,COLOR,LABEL_OR_STYLE,SIZE_FACTOR

#Examples
#internal node with solid branches colored blue and twice the standard width
#9031|9606,clade,#0000ff,normal,2
#internal node with dashed branches colored red and one half the standard width
#601|340,clade,#ff0000,dashed,0.5
#a single internal branch colored green, dashed and 5 times the normal width
#915|777,branch,#00ff00,dashed,5

#colored range covering all leaves of an internal node, colored red and with label 'Eukaryota'
#184922|9606,range,#ff0000,Eukaryota
#examples of colored ranges from iTOL's Tree of Life
#2190|2287,range,#aaffaa,Archaea
#623|1502,range,#aaaaff,Bacteria

#leaf label for node 9606 will be displayed in green, bold and twice the regular font size
#9606,label,#00ff00,bold,2

#leaf label for node 9031 will be displayed in yellow, bold italic and half the regular font size
#9031,label,#ffff00,bold-italic,0.5

#leaf label for node 8015 will be displayed in blue
#8015,label,#0000ff

B000014078	range	#fc0a02	Alcaligenaceae
B000014081	range	#fc0a02	Alcaligenaceae
B000013653	range	#fc0a02	Alcaligenaceae
B000016928	range	#fc0a02	Alcaligenaceae
B000025605	range	#ff025a	Hydrogenophilaceae
B000025607	range	#ff025a	Hydrogenophilaceae
B000003944	range	#ff025a	Hydrogenophilaceae
B000014027	range	#fc0a02	Alcaligenaceae
B000000529	range	#fc0a02	Alcaligenaceae
B000016932	range	#fc0a02	Alcaligenaceae
B000025975	range	#fc0a02	Alcaligenaceae
B000025966	range	#fc0a02	Alcaligenaceae
B000025964	range	#fc0a02	Alcaligenaceae
B000025963	range	#fc0a02	Alcaligenaceae
B000025974	range	#fc0a02	Alcaligenaceae
B000014029	range	#fc0a02	Alcaligenaceae
B000014026	range	#fc0a02	Alcaligenaceae
B000014075	range	#fc0a02	Alcaligenaceae
B000025840	range	#fc0a02	Alcaligenaceae
B000025838	range	#fc0a02	Alcaligenaceae
B000025841	range	#fc0a02	Alcaligenaceae
B000025839	range	#fc0a02	Alcaligenaceae
B000011956	range	#ffffff	Neisseriaceae
B000013946	range	#ffffff	Neisseriaceae
B000022233	range	#ffffff	Rhodocyclaceae
B000022221	range	#ffffff	Rhodocyclaceae
B000005202	range	#ffffff	Rhodocyclaceae
B000022223	range	#ffffff	Rhodocyclaceae
B000022220	range	#ffffff	Rhodocyclaceae
B000022214	range	#ffffff	Rhodocyclaceae
B000022215	range	#ffffff	Rhodocyclaceae
B000007934	range	#ffffff	Rhodocyclaceae
B000007930	range	#ffffff	Rhodocyclaceae
B000022234	range	#ffffff	Rhodocyclaceae
B000030582	range	#ffffff	Rhodocyclaceae
B000024155	range	#ffffff	Neisseriaceae
B000022435	range	#ffffff	Burkholderiales_unclassified
B000022433	range	#ffffff	Burkholderiales_unclassified
B000022385	range	#01aa07	Burkholderiaceae
B000022380	range	#01aa07	Burkholderiaceae
B000030181	range	#01aa07	Burkholderiaceae
B000030187	range	#01aa07	Burkholderiaceae
B000030185	range	#01aa07	Burkholderiaceae
B000008084	range	#01aa07	Burkholderiaceae
B000008085	range	#01aa07	Burkholderiaceae
B000022362	range	#01aa07	Burkholderiaceae
B000000550	range	#01aa07	Burkholderiaceae
B000022427	range	#01aa07	Burkholderiaceae
B000022368	range	#01aa07	Burkholderiaceae
B000022426	range	#01aa07	Burkholderiaceae
B000022338	range	#01aa07	Burkholderiaceae
B000022335	range	#01aa07	Burkholderiaceae
B000022337	range	#01aa07	Burkholderiaceae
B000030175	range	#01aa07	Burkholderiaceae
B000022403	range	#01aa07	Burkholderiaceae
B000022413	range	#01aa07	Burkholderiaceae
B000030176	range	#01aa07	Burkholderiaceae
B000022393	range	#01aa07	Burkholderiaceae
B000022391	range	#01aa07	Burkholderiaceae
B000022365	range	#01aa07	Burkholderiaceae
B000022364	range	#01aa07	Burkholderiaceae
B000022366	range	#01aa07	Burkholderiaceae
B000022382	range	#01aa07	Burkholderiaceae
B000022430	range	#01aa07	Burkholderiaceae
B000030180	range	#01aa07	Burkholderiaceae
B000030182	range	#01aa07	Burkholderiaceae
B000024366	range	#01aa07	Burkholderiaceae
B000030591	range	#01aa07	Burkholderiaceae
B000031794	range	#ffffff	Betaproteobacteria_unclassified
B000024368	range	#01aa07	Burkholderiaceae
B000030590	range	#01aa07	Burkholderiaceae
B000024370	range	#01aa07	Burkholderiaceae
B000030441	range	#01aa07	Burkholderiaceae
B000030076	range	#01aa07	Burkholderiaceae
B000022053	range	#ffffff	Burkholderiales_unclassified
B000024180	range	#01aa07	Burkholderiaceae
B000030442	range	#01aa07	Burkholderiaceae
B000012686	range	#fc0a02	Alcaligenaceae
B000024955	range	#fc0a02	Alcaligenaceae
B000024957	range	#fc0a02	Alcaligenaceae
B000024920	range	#fc0a02	Alcaligenaceae
B000012605	range	#fc0a02	Alcaligenaceae
B000024921	range	#fc0a02	Alcaligenaceae
B000024924	range	#fc0a02	Alcaligenaceae
B000012681	range	#fc0a02	Alcaligenaceae
B000024933	range	#fc0a02	Alcaligenaceae
B000024888	range	#fc0a02	Alcaligenaceae
B000012610	range	#fc0a02	Alcaligenaceae
B000012611	range	#fc0a02	Alcaligenaceae
B000024887	range	#fc0a02	Alcaligenaceae
B000024886	range	#fc0a02	Alcaligenaceae
B000012604	range	#fc0a02	Alcaligenaceae
B000012607	range	#fc0a02	Alcaligenaceae
B000024932	range	#fc0a02	Alcaligenaceae
B000012609	range	#fc0a02	Alcaligenaceae
B000024945	range	#fc0a02	Alcaligenaceae
B000024946	range	#fc0a02	Alcaligenaceae
B000012691	range	#fc0a02	Alcaligenaceae
B000030452	range	#ffffff	Betaproteobacteria_unclassified
B000030454	range	#ffffff	Betaproteobacteria_unclassified
B000025570	range	#ff025a	Hydrogenophilaceae
B000025569	range	#ff025a	Hydrogenophilaceae
B000016927	range	#ff025a	Hydrogenophilaceae
B000024230	range	#ffffff	Betaproteobacteria_unclassified
B000007902	range	#ffffff	Betaproteobacteria_unclassified
B000024346	range	#ffffff	Betaproteobacteria_unclassified
B000004480	range	#ffffff	Betaproteobacteria_unclassified
B000031795	range	#ff025a	Hydrogenophilaceae
B000024146	range	#b301bc	Comamonadaceae
B000024062	range	#b301bc	Comamonadaceae
B000024063	range	#b301bc	Comamonadaceae
B000024113	range	#b301bc	Comamonadaceae
B000024071	range	#b301bc	Comamonadaceae
B000024112	range	#b301bc	Comamonadaceae
B000024111	range	#b301bc	Comamonadaceae
B000002853	range	#b301bc	Comamonadaceae
B000030433	range	#b301bc	Comamonadaceae
B000030434	range	#b301bc	Comamonadaceae
B000030417	range	#b301bc	Comamonadaceae
B000030418	range	#b301bc	Comamonadaceae
B000024117	range	#b301bc	Comamonadaceae
B000024121	range	#b301bc	Comamonadaceae
B000024149	range	#b301bc	Comamonadaceae
B000024096	range	#b301bc	Comamonadaceae
B000011895	range	#b301bc	Comamonadaceae
B000024147	range	#b301bc	Comamonadaceae
B000024197	range	#b301bc	Comamonadaceae
B000022026	range	#b301bc	Comamonadaceae
B000024089	range	#b301bc	Comamonadaceae
B000030397	range	#b301bc	Comamonadaceae
B000011881	range	#b301bc	Comamonadaceae
B000011906	range	#b301bc	Comamonadaceae
B000030665	range	#b301bc	Comamonadaceae
B000006743	range	#b301bc	Comamonadaceae
B000024084	range	#b301bc	Comamonadaceae
B000024082	range	#b301bc	Comamonadaceae
B000030398	range	#b301bc	Comamonadaceae
B000024103	range	#b301bc	Comamonadaceae
B000030409	range	#b301bc	Comamonadaceae
B000030410	range	#b301bc	Comamonadaceae
B000011893	range	#b301bc	Comamonadaceae
B000011888	range	#b301bc	Comamonadaceae
B000024195	range	#b301bc	Comamonadaceae
B000031759	range	#b301bc	Comamonadaceae
B000031755	range	#b301bc	Comamonadaceae
B000031746	range	#b301bc	Comamonadaceae
B000030079	range	#b301bc	Comamonadaceae
B000030077	range	#b301bc	Comamonadaceae
B000030078	range	#b301bc	Comamonadaceae
B000030075	range	#b301bc	Comamonadaceae
B000007739	range	#b301bc	Comamonadaceae
B000007740	range	#b301bc	Comamonadaceae
B000007742	range	#b301bc	Comamonadaceae
B000022065	range	#b301bc	Comamonadaceae
B000022066	range	#b301bc	Comamonadaceae
B000022193	range	#b301bc	Comamonadaceae
B000031741	range	#b301bc	Comamonadaceae
B000030721	range	#b301bc	Comamonadaceae
B000030141	range	#ffffff	Burkholderiales_unclassified
B000022080	range	#b301bc	Comamonadaceae
B000030142	range	#b301bc	Comamonadaceae
B000030666	range	#b301bc	Comamonadaceae
B000024659	range	#b301bc	Comamonadaceae
B000030731	range	#b301bc	Comamonadaceae
B000030710	range	#b301bc	Comamonadaceae
B000024662	range	#b301bc	Comamonadaceae
B000030707	range	#b301bc	Comamonadaceae
B000030720	range	#b301bc	Comamonadaceae
B000022081	range	#b301bc	Comamonadaceae
B000022068	range	#b301bc	Comamonadaceae
B000022112	range	#b301bc	Comamonadaceae
B000022113	range	#b301bc	Comamonadaceae
B000030051	range	#b301bc	Comamonadaceae
B000021984	range	#b301bc	Comamonadaceae
B000030732	range	#b301bc	Comamonadaceae
B000030699	range	#b301bc	Comamonadaceae
B000030698	range	#b301bc	Comamonadaceae
B000024102	range	#b301bc	Comamonadaceae
B000024657	range	#b301bc	Comamonadaceae
B000024658	range	#b301bc	Comamonadaceae
B000021982	range	#b301bc	Comamonadaceae
B000000524	range	#b301bc	Comamonadaceae
B000021980	range	#b301bc	Comamonadaceae
B000022010	range	#b301bc	Comamonadaceae
B000022004	range	#b301bc	Comamonadaceae
B000021983	range	#b301bc	Comamonadaceae
B000021986	range	#b301bc	Comamonadaceae
B000030054	range	#b301bc	Comamonadaceae
B000022489	range	#b301bc	Comamonadaceae
B000008230	range	#b301bc	Comamonadaceae
B000030733	range	#b301bc	Comamonadaceae
B000022485	range	#b301bc	Comamonadaceae
B000008231	range	#b301bc	Comamonadaceae
B000008229	range	#b301bc	Comamonadaceae
B000008228	range	#b301bc	Comamonadaceae
B000008233	range	#b301bc	Comamonadaceae
B000030134	range	#b301bc	Comamonadaceae
B000030136	range	#b301bc	Comamonadaceae
B000030117	range	#b301bc	Comamonadaceae
B000031761	range	#b301bc	Comamonadaceae
B000031762	range	#b301bc	Comamonadaceae
B000022137	range	#b301bc	Comamonadaceae
B000022142	range	#b301bc	Comamonadaceae
B000030155	range	#b301bc	Comamonadaceae
B000007780	range	#b301bc	Comamonadaceae
B000007782	range	#b301bc	Comamonadaceae
B000022190	range	#b301bc	Comamonadaceae
B000022122	range	#b301bc	Comamonadaceae
B000022120	range	#b301bc	Comamonadaceae
B000030164	range	#b301bc	Comamonadaceae
B000030165	range	#b301bc	Comamonadaceae
B000030163	range	#b301bc	Comamonadaceae
B000000530	range	#b301bc	Comamonadaceae
B000030170	range	#b301bc	Comamonadaceae
B000030158	range	#b301bc	Comamonadaceae
B000030116	range	#b301bc	Comamonadaceae
B000030097	range	#b301bc	Comamonadaceae
B000022136	range	#b301bc	Comamonadaceae
B000022141	range	#b301bc	Comamonadaceae
B000030172	range	#b301bc	Comamonadaceae
B000030156	range	#b301bc	Comamonadaceae
B000030098	range	#b301bc	Comamonadaceae
B000030095	range	#b301bc	Comamonadaceae
B000022051	range	#b301bc	Comamonadaceae
B000022029	range	#b301bc	Comamonadaceae
B000030056	range	#b301bc	Comamonadaceae
B000030057	range	#b301bc	Comamonadaceae
B000030055	range	#b301bc	Comamonadaceae
B000007732	range	#b301bc	Comamonadaceae
B000022052	range	#b301bc	Comamonadaceae
B000024177	range	#b301bc	Comamonadaceae
B000020110	range	#b301bc	Comamonadaceae
B000024191	range	#b301bc	Comamonadaceae
B000024579	range	#b301bc	Comamonadaceae
B000024582	range	#b301bc	Comamonadaceae
B000024583	range	#b301bc	Comamonadaceae
B000024584	range	#b301bc	Comamonadaceae
B000031827	range	#ffffff	Burkholderiales_unclassified
B000031829	range	#ffffff	Burkholderiales_unclassified
B000012750	range	#ffffff	Burkholderiales_unclassified
B000024279	range	#01aa07	Burkholderiaceae
B000003075	range	#ffffff	Oxalobacteraceae
B000024275	range	#ffffff	Oxalobacteraceae
B000024278	range	#ffffff	Oxalobacteraceae
B000024276	range	#ffffff	Oxalobacteraceae
B000024280	range	#ffffff	Oxalobacteraceae
B000024277	range	#ffffff	Oxalobacteraceae
B000024281	range	#ffffff	Oxalobacteraceae
B000024220	range	#01aa07	Burkholderiaceae
B000024217	range	#01aa07	Burkholderiaceae
B000007970	range	#ffffff	Oxalobacteraceae
B000030610	range	#ffffff	Oxalobacteraceae
B000024367	range	#ffffff	Neisseriaceae
B000007972	range	#ffffff	Neisseriaceae
B000007689	range	#ffffff	Neisseriaceae
B000031967	range	#ffffff	Betaproteobacteria_unclassified
B000031968	range	#ffffff	Betaproteobacteria_unclassified
B000025660	range	#ffffff	Ferrovaceae
B000025655	range	#ffffff	Ferrovaceae
B000013712	range	#ffffff	Ferrovaceae
B000025658	range	#ffffff	Ferrovaceae
B000003929	range	#ffffff	Betaproteobacteria_unclassified
B000024207	range	#ffffff	Neisseriaceae
B000007976	range	#42e8f4	Methylophilaceae
B000022240	range	#42e8f4	Methylophilaceae
B000022241	range	#42e8f4	Methylophilaceae
B000008813	range	#42e8f4	Methylophilaceae
B000007971	range	#42e8f4	Methylophilaceae
B000022264	range	#42e8f4	Methylophilaceae
B000022266	range	#42e8f4	Methylophilaceae
B000030526	range	#42e8f4	Methylophilaceae
B000030525	range	#42e8f4	Methylophilaceae
B000012359	range	#42e8f4	Methylophilaceae
B000024543	range	#42e8f4	Methylophilaceae
B000024545	range	#42e8f4	Methylophilaceae
B000024542	range	#42e8f4	Methylophilaceae
B000024541	range	#42e8f4	Methylophilaceae
B000031809	range	#42e8f4	Methylophilaceae
B000031808	range	#42e8f4	Methylophilaceae
B000012544	range	#42e8f4	Methylophilaceae
B000012547	range	#42e8f4	Methylophilaceae
B000012548	range	#42e8f4	Methylophilaceae
B000024204	range	#42e8f4	Methylophilaceae
B000030556	range	#42e8f4	Methylophilaceae
B000031817	range	#42e8f4	Methylophilaceae
B000031816	range	#42e8f4	Methylophilaceae
B000011981	range	#42e8f4	Methylophilaceae
B000024305	range	#42e8f4	Methylophilaceae
B000024301	range	#42e8f4	Methylophilaceae
B000030564	range	#42e8f4	Methylophilaceae
B000030565	range	#42e8f4	Methylophilaceae
B000030557	range	#42e8f4	Methylophilaceae
B000024302	range	#42e8f4	Methylophilaceae
B000024308	range	#42e8f4	Methylophilaceae
B000030583	range	#ffffff	Betaproteobacteria_unclassified
B000030584	range	#ffffff	Betaproteobacteria_unclassified
B000011694	range	#ffffff	Betaproteobacteria_unclassified
B000011691	range	#ffffff	Betaproteobacteria_unclassified
B000011690	range	#ffffff	Betaproteobacteria_unclassified
B000011692	range	#ffffff	Betaproteobacteria_unclassified
B000011699	range	#ffffff	Betaproteobacteria_unclassified
B000011700	range	#ffffff	Betaproteobacteria_unclassified
B000030461	range	#ffffff	Gallionellaceae
B000011959	range	#ffffff	Rhodocyclaceae
B000030460	range	#ffffff	Gallionellaceae
B000030459	range	#ffffff	Gallionellaceae
B000011964	range	#ffffff	Nitrosomonadaceae
B000030577	range	#ffffff	Betaproteobacteria_unclassified
B000030578	range	#ffffff	Betaproteobacteria_unclassified
B000024345	range	#ffffff	Betaproteobacteria_unclassified
B000025812	range	#ffffff	Gallionellaceae
B000007901	range	#ffffff	Gallionellaceae
B000007900	range	#ffffff	Gallionellaceae
B000030585	range	#ffffff	Betaproteobacteria_unclassified
B000025627	range	#ffffff	TRA3-20_fa
B000025626	range	#ffffff	TRA3-20_fa
B000025615	range	#ffffff	TRA3-20_fa
B000025616	range	#ffffff	TRA3-20_fa
B000025590	range	#ffffff	TRA3-20_fa
B000025588	range	#ffffff	TRA3-20_fa
B000025587	range	#ffffff	TRA3-20_fa
B000030475	range	#ffffff	Nitrosomonadaceae
B000002858	range	#ffffff	Betaproteobacteria_unclassified
B000030492	range	#ffffff	Nitrosomonadaceae
B000030491	range	#ffffff	Nitrosomonadaceae
B000030493	range	#ffffff	Betaproteobacteria_unclassified
B000030498	range	#ffffff	Betaproteobacteria_unclassified
B000031784	range	#ffffff	Nitrosomonadaceae
B000031786	range	#ffffff	Nitrosomonadaceae
B000030473	range	#ffffff	Nitrosomonadaceae
B000030464	range	#ffffff	Rhodocyclaceae
B000013944	range	#ffffff	Rhodocyclaceae
B000030512	range	#ffffff	Rhodocyclaceae
B000022334	range	#ffffff	Rhodocyclaceae
B000030513	range	#ffffff	Rhodocyclaceae
B000025816	range	#ffffff	Rhodocyclaceae
B000003987	range	#ffffff	Rhodocyclaceae
B000013451	range	#ffffff	Betaproteobacteria_unclassified
B000013448	range	#ffffff	Betaproteobacteria_unclassified
B000021583	range	#ffffff	Nitrosomonadaceae
