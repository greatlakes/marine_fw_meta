DATASET_BINARY
#Binary datasets are visualized as filled or empty symbols, depending on the value associated with a node (0 or 1).
#Each node can have multiple associated values, and each value will be represented by a symbol (defined in FIELD_SHAPES) with corresponding color and label (from FIELD_COLORS and FIELD_LABELS).
#Possible values (defined under DATA below) for each node are 1 (filled shapes), 0 (empty shapes) and -1 (completely omitted).

#lines starting with a hash are comments and ignored during parsing

#=================================================================#
#                    MANDATORY SETTINGS                           #
#=================================================================#
#select the separator which is used to delimit the data below (TAB,SPACE or COMMA).This separator must be used throughout this file (except in the SEPARATOR line, which uses space).
SEPARATOR TAB
#SEPARATOR SPACE
#SEPARATOR COMMA

#label is used in the legend table (can be changed later)
DATASET_LABEL	habitat

#dataset color (can be changed later)
COLOR	#ff0000

#shapes for each field column; possible choices are
#1: rectangle 
#2: circle
#3: star
#4: right pointing triangle
#5: left pointing triangle
#6: check mark
FIELD_SHAPES	1	1	1

#field labels
FIELD_LABELS	marine	freshwater	shared

#=================================================================#
#                    OPTIONAL SETTINGS                            #
#=================================================================#

#define colors for each individual field column (if not defined all symbols will use the main dataset color, defined in COLOR above)
FIELD_COLORS	#0008ff	#37ff00	#ff8800


#=================================================================#
#     all other optional settings can be set or changed later     #
#           in the web interface (under 'Datasets' tab)           #
#=================================================================#

#always show internal values; if set, values associated to internal nodes will be displayed even if these nodes are not collapsed. It could cause overlapping in the dataset display.
#SHOW_INTERNAL,0

#left margin, used to increase/decrease the spacing to the next dataset. Can be negative, causing datasets to overlap.
#MARGIN,0

#symbol height factor; Default symbol height will be slightly less than the available space between leaves, but you can set a multiplication factor here to increase/decrease it (values from 0 to 1 will decrease it, values above 1 will increase it)
#HEIGHT_FACTOR,1

#increase/decrease the spacing between individual levels, when there is more than one binary level defined 
#SYMBOL_SPACING,10

#display or hide the text labels above each field column
#SHOW_LABELS,1

#Example dataset with 4 columns (circle, left triangle, right triangle and rectangle):
#FIELD_SHAPES,2,4,5,1
#FIELD_LABELS,f1,f2,f3,f4
#FIELD_COLORS,#ff0000,#00ff00,#ffff00,#0000ff

#Internal tree nodes can be specified using IDs directly, or using the 'last common ancestor' method described in iTOL help pages
#=================================================================#
#       Actual data follows after the DATA keyword              #
#=================================================================#
DATA
#node 9606 will have a filled circle, empty left triangle, nothing in the 3rd column and an empty rectangle
#9606,1,0,-1,0

B000027489	-1	0	-1
B000027487	-1	0	-1
B000007133	-1	0	-1
B000005316	-1	0	-1
B000029670	-1	0	-1
B000029675	-1	0	-1
B000027503	-1	0	-1
B000027509	-1	0	-1
B000019666	-1	0	-1
B000027511	-1	0	-1
B000028883	-1	0	-1
B000027510	-1	0	-1
B000019667	-1	0	-1
B000031520	-1	0	-1
B000031519	-1	0	-1
B000027514	-1	0	-1
B000017174	-1	0	-1
B000027501	-1	0	-1
B000027502	-1	0	-1
B000017173	-1	0	-1
B000029681	-1	0	-1
B000029679	-1	0	-1
B000029660	-1	0	-1
B000029656	-1	0	-1
B000020709	-1	0	-1
B000027558	-1	0	-1
B000028977	-1	0	-1
B000019832	-1	0	-1
B000019934	0	-1	-1
B000017506	0	-1	-1
B000019865	0	-1	-1
B000028978	-1	0	-1
B000019951	0	-1	-1
B000019952	-1	0	-1
B000028976	0	-1	-1
B000031162	0	-1	-1
B000027555	0	-1	-1
B000031164	0	-1	-1
B000024804	-1	0	-1
B000024807	-1	0	-1
B000000366	-1	0	-1
B000012466	-1	0	-1
B000030775	-1	0	-1
B000030767	-1	0	-1
B000012454	-1	0	-1
B000012452	-1	0	-1
B000007486	-1	0	-1
B000007485	-1	0	-1
B000024794	-1	0	-1
B000012667	-1	0	-1
B000024912	-1	0	-1
B000024907	-1	0	-1
B000012670	-1	0	-1
B000012470	-1	0	-1
B000030768	-1	0	-1
B000024791	-1	0	-1
B000030774	-1	0	-1
B000012471	-1	0	-1
B000024761	-1	0	-1
B000024760	-1	0	-1
B000024762	-1	0	-1
B000024763	-1	0	-1
B000012458	-1	0	-1
B000024777	-1	0	-1
B000024781	-1	0	-1
B000024749	-1	0	-1
B000024753	-1	0	-1
B000012469	-1	0	-1
B000009379	0	-1	-1
B000026875	0	-1	-1
B000026866	0	-1	-1
B000009384	0	-1	-1
B000026868	0	-1	-1
B000026876	0	-1	-1
B000026860	0	-1	-1
B000026862	0	-1	-1
B000005321	-1	0	-1
B000015511	-1	0	-1
B000028456	-1	0	-1
B000028457	-1	0	-1
B000009323	-1	0	-1
B000009319	-1	0	-1
B000012577	-1	0	-1
B000022924	-1	0	-1
B000022926	-1	0	-1
B000009347	-1	0	-1
B000009595	-1	0	-1
B000009593	-1	0	-1
B000005065	-1	0	-1
B000015906	-1	0	-1
B000015914	-1	0	-1
B000026927	-1	0	-1
B000026932	-1	0	-1
B000026879	-1	0	-1
B000026882	-1	0	-1
B000026928	-1	0	-1
B000015907	-1	0	-1
B000031128	-1	0	-1
B000031130	-1	0	-1
B000001182	-1	0	-1
B000009015	-1	0	-1
B000022729	-1	0	-1
B000022730	-1	0	-1
B000030209	-1	0	-1
B000022751	-1	0	-1
B000030210	-1	0	-1
B000022752	-1	0	-1
B000022757	-1	0	-1
B000022749	-1	0	-1
B000022742	-1	0	-1
B000022743	-1	0	-1
B000005722	-1	0	-1
B000008704	0	-1	-1
B000014979	0	-1	-1
B000015244	0	-1	-1
B000015248	0	-1	-1
B000015243	0	-1	-1
B000008805	0	-1	-1
B000001255	0	-1	-1
B000008708	0	-1	-1
B000008706	0	-1	-1
B000009164	-1	0	-1
B000009161	-1	0	-1
B000009163	-1	0	-1
B000008740	-1	0	-1
B000024013	-1	0	-1
B000024012	-1	0	-1
B000024011	-1	0	-1
B000007591	-1	0	-1
B000024004	-1	0	-1
B000011710	-1	0	-1
B000024002	-1	0	-1
B000002977	-1	0	-1
B000022473	-1	0	-1
B000022474	-1	0	-1
B000001184	-1	0	-1
B000008742	-1	0	-1
B000030215	-1	0	-1
B000030213	-1	0	-1
B000022770	-1	0	-1
B000009042	-1	0	-1
B000022796	-1	0	-1
B000022797	-1	0	-1
B000009194	-1	0	-1
B000022544	-1	0	-1
B000022547	-1	0	-1
B000009057	-1	0	-1
B000009056	-1	0	-1
B000009120	0	-1	-1
B000016997	-1	0	-1
B000009193	-1	0	-1
B000004568	-1	0	-1
B000004530	0	-1	-1
B000015636	-1	0	-1
B000026658	-1	0	-1
B000026659	-1	0	-1
B000026645	-1	0	-1
B000026643	-1	0	-1
B000005072	-1	0	-1
B000029837	-1	0	-1
B000017409	-1	0	-1
B000017404	-1	0	-1
B000017411	-1	0	-1
B000026695	-1	0	-1
B000017378	-1	0	-1
B000017379	-1	0	-1
B000017444	-1	0	-1
B000017380	-1	0	-1
B000017445	-1	0	-1
B000017087	-1	0	-1
B000017089	-1	0	-1
B000027455	-1	0	-1
B000017090	1	1	1
B000027451	-1	0	-1
B000004708	-1	0	-1
B000026761	-1	0	-1
B000026760	-1	0	-1
B000026705	-1	0	-1
B000015639	-1	0	-1
B000026669	-1	0	-1
B000031123	0	-1	-1
B000005051	0	-1	-1
B000031122	0	-1	-1
B000017230	0	-1	-1
B000029833	-1	0	-1
B000029845	-1	0	-1
B000029847	-1	0	-1
B000021450	-1	0	-1
B000017381	-1	0	-1
B000027585	-1	0	-1
B000027568	-1	0	-1
B000027570	-1	0	-1
B000027582	-1	0	-1
B000027581	-1	0	-1
B000027580	-1	0	-1
B000017477	-1	0	-1
B000017475	-1	0	-1
B000027623	-1	0	-1
B000027622	-1	0	-1
B000027631	-1	0	-1
B000027612	-1	0	-1
B000027610	-1	0	-1
B000027611	-1	0	-1
B000027632	-1	0	-1
B000017353	-1	0	-1
B000017354	-1	0	-1
B000017356	-1	0	-1
B000017352	-1	0	-1
B000017351	-1	0	-1
B000017311	-1	0	-1
B000027566	-1	0	-1
B000027567	-1	0	-1
B000003000	-1	0	-1
B000026712	-1	0	-1
B000026711	-1	0	-1
B000026749	-1	0	-1
B000026748	-1	0	-1
B000026724	-1	0	-1
B000015634	-1	0	-1
B000026721	-1	0	-1
B000026710	-1	0	-1
B000026738	-1	0	-1
B000026733	-1	0	-1
B000016603	-1	0	-1
B000031144	-1	0	-1
B000027239	-1	0	-1
B000027236	-1	0	-1
B000027237	-1	0	-1
B000016601	-1	0	-1
B000027260	-1	0	-1
B000027261	-1	0	-1
B000027259	-1	0	-1
B000016597	-1	0	-1
B000027264	-1	0	-1
B000016605	-1	0	-1
B000031143	-1	0	-1
B000027266	-1	0	-1
B000027267	-1	0	-1
B000015628	-1	0	-1
B000026693	-1	0	-1
B000026831	-1	0	-1
B000026832	-1	0	-1
B000026829	-1	0	-1
B000015546	-1	0	-1
B000026804	-1	0	-1
B000026838	-1	0	-1
B000026839	-1	0	-1
B000026805	-1	0	-1
B000015478	-1	0	-1
B000015483	-1	0	-1
B000016530	-1	0	-1
B000015602	-1	0	-1
B000026815	-1	0	-1
B000026818	-1	0	-1
B000015542	-1	0	-1
B000016527	-1	0	-1
B000016528	-1	0	-1
B000004706	-1	0	-1
B000004731	-1	0	-1
B000015543	-1	0	-1
B000015605	-1	0	-1
B000015538	-1	0	-1
B000031119	-1	0	-1
B000031118	-1	0	-1
B000005073	-1	0	-1
B000015513	-1	0	-1
B000024717	-1	0	-1
B000012311	-1	0	-1
B000024716	-1	0	-1
B000024715	-1	0	-1
B000012315	1	1	1
B000024725	-1	0	-1
B000024722	-1	0	-1
B000024724	-1	0	-1
B000026690	-1	0	-1
B000026689	-1	0	-1
B000015510	-1	0	-1
B000015537	-1	0	-1
B000015045	-1	0	-1
B000004827	-1	0	-1
B000027277	-1	0	-1
B000027279	-1	0	-1
B000012234	-1	0	-1
B000012235	-1	0	-1
B000012227	-1	0	-1
B000024559	-1	0	-1
B000024560	-1	0	-1
B000012560	-1	0	-1
B000012561	-1	0	-1
B000015840	-1	0	-1
B000015837	-1	0	-1
B000023251	-1	0	-1
B000023250	-1	0	-1
B000005045	-1	0	-1
B000016673	-1	0	-1
B000016672	-1	0	-1
B000016677	-1	0	-1
B000016475	-1	0	-1
B000016469	-1	0	-1
B000005041	0	-1	-1
B000020964	0	-1	-1
B000020961	0	-1	-1
